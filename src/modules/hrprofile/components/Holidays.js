import React, { useState, useEffect, useContext } from "react";
import {
  Input,
  DatePicker,
  Button,
  Divider,
  Table,
  P,
  Modal,
} from "../../AntStyles";
import {
  HolidaysWrapper,
  HolidaysHeader,
  HolidaysList,
  UpdateBtn,
} from "../css/HolidaysStyles";
import { ALL_HOLIDAYS, HOLIDAY } from "../graphql/Queries";
import {
  CREATE_HOLIDAY,
  UPDATE_HOLIDAY,
  DELETE_HOLIDAY,
} from "../graphql/Mutations";
import { useQuery } from "react-apollo";
import client from "../../../apollo";
import moment from "moment";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { GetUserContext } from "../../UserContextProvider";
import UploadHolidayListButton from "./UploadHolidayListButton";

const { Column } = Table;
const { confirm } = Modal;

function Holidays() {
  const { userData } = useContext(GetUserContext);
  const details = userData || {};
  const { companyId = "", role = "" } = details;

  const [loader, setLoader] = useState(false);
  const [editable, setEditable] = useState(false);
  const [holidayId, setHolidayId] = useState("");
  const [inpData, setInpData] = useState({
    name: "",
    date: moment(),
  });

  useEffect(() => {
    if (holidayId) {
      client
        .query({
          query: HOLIDAY,
          variables: { id: holidayId },
        })
        .then((res) => {
          const details = res.data.getHoliday;
          const name = details.name;
          const date = moment(details.date);
          setInpData(() => {
            return { name, date };
          });
        })
        .catch((error) => {
          console.error(error);
        });
    }
  }, [holidayId]);

  const { loading, error, data, refetch } = useQuery(ALL_HOLIDAYS, {
    variables: { companyId },
    fetchPolicy: "network-only",
  });

  if (loading) return <div>loading</div>;
  if (error) return <div>error</div>;

  const allHolidaysList = data.allHolidays;
  let rowdata = [];

  rowdata =
    allHolidaysList && allHolidaysList.length
      ? allHolidaysList.map((holiday, index) => {
          return holidayId === holiday.id && editable
            ? {
                key: holiday.id,
                name: (
                  <Input
                    className="input80"
                    size="large"
                    value={inpData.name}
                    onChange={(e) => {
                      const val = e.target.value;
                      setInpData((prevState) => {
                        return { ...prevState, name: val };
                      });
                    }}
                  />
                ),
                onDate: (
                  <DatePicker
                    size="large"
                    className="dateBorderRadius"
                    format="DD/MM/YYYY"
                    value={inpData && inpData.date}
                    onChange={(e) => {
                      const val = e;
                      setInpData((prevState) => {
                        return { ...prevState, date: val };
                      });
                    }}
                  />
                ),
              }
            : {
                key: holiday.id,
                name: holiday.name,
                onDate: moment(holiday.date).format("DD/MM/YYYY"),
              };
        })
      : "";

  let tabledata = [];
  if (role === "Admin") {
    tabledata = [
      ...rowdata,
      {
        key: rowdata.length,
        name: (
          <Input
            className="input80"
            size="large"
            value={!editable ? inpData.name : ""}
            onChange={(e) => {
              const val = e.target.value;
              setInpData((prevState) => {
                return { ...prevState, name: val };
              });
            }}
          />
        ),
        onDate: (
          <DatePicker
            size="large"
            className="dateBorderRadius"
            format="DD/MM/YYYY"
            value={!editable ? inpData.date : ""}
            onChange={(e) => {
              const val = e;
              setInpData((prevState) => {
                return { ...prevState, date: val };
              });
            }}
          />
        ),
      },
    ];
  } else {
    tabledata = [...rowdata];
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    setLoader(true);
    if (inpData.name && inpData.date) {
      const holidaydate = `${moment(inpData.date).format(
        "YYYY-MM-DD",
      )}T00:00:00.000Z`;

      client
        .mutate({
          mutation: CREATE_HOLIDAY,
          variables: { name: inpData.name, date: holidaydate, companyId },
        })
        .then((res) => {
          refetch();
          setInpData(() => {
            return { name: "", date: moment() };
          });
          Modal.success({
            content: "Holiday added successfully",
          });
          setLoader(false);
        })
        .catch((error) => {
          console.log(error);
          Modal.error({
            title: "Holiday can not be added",
          });
          setLoader(false);
        });
    } else {
      Modal.error({
        title: "Invalid Data",
        content: "Input valid name and date",
      });
      setLoader(false);
    }
  };

  const handleDelete = (props) => {
    const holidayId = props;
    confirm({
      title: "Are you sure delete this holiday?",
      okText: "Yes",
      okType: "danger",
      cancelText: "No",
      onOk() {
        client
          .mutate({
            mutation: DELETE_HOLIDAY,
            variables: { id: holidayId },
          })
          .then((res) => {
            refetch();
            Modal.success({
              content: "Holiday Deleted successfully",
            });
          })
          .catch((error) => {
            console.log(error);
            Modal.error({
              content: "Holiday can not be deleted",
            });
          });
      },
      onCancel() {},
    });
  };

  const handleEdit = (props) => {
    setHolidayId(props);
    setEditable(true);
  };

  const handleUpdate = () => {
    setLoader(true);
    if (inpData.name && inpData.date) {
      const date = inpData.date;
      const name = inpData.name;
      client
        .mutate({
          mutation: UPDATE_HOLIDAY,
          variables: { name, date, companyId, id: holidayId },
        })
        .then((res) => {
          refetch();
          setInpData(() => {
            return { name: "", date: moment() };
          });
          Modal.success({
            content: "Holiday Updated successfully",
          });
          setEditable(false);
          setLoader(false);
        })
        .catch((error) => {
          console.log(error);
          Modal.error({
            content: "Holiday can not be updated",
          });
          setLoader(false);
        });
    } else {
      Modal.error({
        title: "Invalid Data",
        content: "Input valid name and date",
      });
      setLoader(false);
    }
  };

  const handleUpdateCancel = () => {
    setEditable(false);
    setHolidayId("");
    setInpData({
      name: "",
      date: moment(),
    });
  };

  const handleCancel = () => {
    setInpData({
      name: "",
      date: moment(),
    });
  };

  return (
    <HolidaysWrapper>
      <HolidaysHeader>
        <div>
          <P fontsize="18px">Manage Holidays</P>
        </div>
        <div>
          <UploadHolidayListButton />
        </div>
      </HolidaysHeader>
      <Divider />
      <HolidaysList>
        <Table dataSource={tabledata} size="middle" pagination={false}>
          <Column
            title={<b>Name</b>}
            dataIndex="name"
            key="name"
            width="20%"
            className="table_left_border"
          />
          <Column
            title={<b>On Date</b>}
            dataIndex="onDate"
            key="onDate"
            className={role && role !== "Admin" ? "table_right_border" : ""}
          />
          {role && role === "Admin" && (
            <Column
              title={<b>Actions</b>}
              dataIndex="actions"
              key="actions"
              className="table_right_border"
              width="8%"
              render={(text, record) => {
                if (record.key !== tabledata.length - 1) {
                  return holidayId === record.key && editable ? (
                    <UpdateBtn>
                      <Button
                        className="updtbtn"
                        color="#FFFFFF"
                        bordercolor="#3C3C84"
                        bgcolor="#3C3C84"
                        onClick={handleUpdate}
                        loading={loader}
                      >
                        Update
                      </Button>
                      <Button onClick={handleUpdateCancel}>Cancel</Button>
                    </UpdateBtn>
                  ) : (
                    <span>
                      <DeleteOutlined
                        onClick={() => handleDelete(record.key)}
                        style={{ padding: "0px 10px" }}
                      />
                      <EditOutlined
                        onClick={() => handleEdit(record.key)}
                        style={{ padding: "0px 10px" }}
                      />
                    </span>
                  );
                } else {
                  return (
                    <UpdateBtn>
                      <Button
                        color="#FFFFFF"
                        bordercolor="#3C3C84"
                        bgcolor="#3C3C84"
                        onClick={handleSubmit}
                        loading={loader}
                        className="updtbtn"
                      >
                        Add
                      </Button>
                      <Button onClick={handleCancel}>Cancel</Button>
                    </UpdateBtn>
                  );
                }
              }}
            />
          )}
        </Table>
      </HolidaysList>
    </HolidaysWrapper>
  );
}

export default Holidays;
