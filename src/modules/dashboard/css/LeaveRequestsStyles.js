import styled from "styled-components";

export const flex = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
  font-family: "Poppins", sans-serif;
`;

export const LeaveReqWrapper = styled(flex)`
  flex-direction: column;
  background-color: #f0f0fa;
  padding: 5% 5%;
  border-radius: 4px;
  height: 500px;
  width: 100%;
`;

export const LeaveReqHeader = styled(flex)`
  justify-content: space-between;
`;

export const LeaveType = styled.div`
  padding: 2%;
  font-family: "Poppins", sans-serif;
`;

export const DateTime = styled(flex)`
  justify-content: space-between;
  padding: 2%;
`;
