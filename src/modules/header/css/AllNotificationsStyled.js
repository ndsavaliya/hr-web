import styled from "styled-components";

const flex = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
  font-family: "Poppins", sans-serif;
`;

export const AllNotificationWrapper = styled(flex)`
  flex-direction: column;
  padding: 10px 20px;
`;

export const NotificationHeader = styled(flex)`
  justify-content: space-between;
  align-content: space-between;
`;

export const NotificationBar = styled(flex)`
  flex-direction: column;
  margin: 10px;
  background-color: #f0f0fa;
  padding: 10px 10px;
  border-radius: 4px;
`;

export const NotificationTitle = styled(flex)`
  justify-content: space-between;
  align-items: center;
  align-content: space-between;
`;
