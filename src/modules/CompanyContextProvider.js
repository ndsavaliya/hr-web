import React, { useEffect, useState, useContext } from "react";
import client from "../apollo";
import { GET_COMPANY } from "./company/graphql/Queries";
import { GetUserContext } from "./UserContextProvider";

const GetCompanyContext = React.createContext(null);

function CompanyContextProvider(props) {
  const { userData } = useContext(GetUserContext);
  const details = userData || {};
  const { companyId = "" } = details;

  const [companyData, setCompanyData] = useState("");

  useEffect(() => {
    if (companyId) {
      client
        .query({
          query: GET_COMPANY,
          variables: { id: companyId },
        })
        .then(res => {
          setCompanyData(res.data.getCompany);
        })
        .catch(e => {
          console.log(e);
        });
    }
  }, [companyId]);

  return (
    <GetCompanyContext.Provider value={{ companyData, setCompanyData }}>
      {props.children}
    </GetCompanyContext.Provider>
  );
}

export { CompanyContextProvider, GetCompanyContext };
