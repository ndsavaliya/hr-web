import React, { useState, useContext } from "react";
import {
  Input,
  Button,
  Divider,
  Table,
  P,
  Modal,
  Select,
} from "../../AntStyles";
import {
  LeavesWrapper,
  LeavesHeader,
  LeavesList,
  UpdateBtn,
} from "../css/LeavesStyles";
import { LEAVE_SETTING } from "../graphql/Queries";
import { CREATE_LEAVES, DELETE_LEAVES } from "../graphql/Mutations";
// import { useQuery } from "react-apollo";
import client from "../../../apollo";
import { DeleteOutlined } from "@ant-design/icons";
import { GetUserContext } from "../../UserContextProvider";
import { useQuery } from "react-apollo";
// import UploadHolidayListButton from "./UploadHolidayListButton";
import { startCase, capitalize } from "lodash";

const { Column } = Table;
const { confirm } = Modal;
const { Option } = Select;

function Leaves() {
  const { userData } = useContext(GetUserContext);
  const details = userData || {};
  const { companyId = "", role = "" } = details;

  const [loader, setLoader] = useState(false);
  const [inpData, setInpData] = useState({
    leaves: "",
    paidUnpaid: "",
    noOfLeaves: 0,
  });

  const { loading, error, data, refetch } = useQuery(LEAVE_SETTING, {
    variables: { companyId },
    fetchPolicy: "network-only",
  });
  console.log(error);
  if (loading) return <div>loading</div>;
  if (error) return <div>error</div>;
  // console.log(data);

  const allLeavesList = data && data.getLeaveSetting;
  // console.log(allLeavesList);

  let rowdata = [];
  rowdata = (allLeavesList || []).flatMap((leaves) =>
    leaves
      ? (leaves.leaveTypes || []).map((_leaveType) => ({
          key: _leaveType.id,
          leaves: startCase(_leaveType.type),
          paidUnpaid: capitalize(_leaveType.isPaid),
          noOfLeaves: _leaveType.totalLeaves,
        }))
      : [],
  );

  // console.log(rowdata);

  let tabledata = [];
  if (role === "Admin") {
    tabledata = [
      ...rowdata,
      {
        key: rowdata.length,
        leaves: (
          <Input
            className="input80"
            size="large"
            value={inpData.leaves}
            onChange={(e) => {
              const val = e.target.value;
              setInpData((prevState) => {
                return { ...prevState, leaves: val };
              });
            }}
          />
        ),
        paidUnpaid: (
          <Select
            size="large"
            className="selectBorderRadius"
            value={inpData.paidUnpaid}
            onChange={(e) => {
              const val = e;
              setInpData((prevState) => {
                return { ...prevState, paidUnpaid: val };
              });
            }}
          >
            <Option value="PAID">Paid</Option>
            <Option value="UNPAID">Unpaid</Option>
          </Select>
        ),
        noOfLeaves: (
          <Input
            className="input50"
            size="large"
            value={inpData.noOfLeaves}
            onChange={(e) => {
              const val = parseInt(e.target.value || 0, 10);
              if (Number.isNaN(val)) {
                return;
              } else {
                setInpData((prevState) => {
                  return { ...prevState, noOfLeaves: val };
                });
              }
            }}
          />
        ),
      },
    ];
  } else {
    tabledata = [...rowdata];
  }

  const handleSubmit = (e) => {
    // console.log(inpData);
    e.preventDefault();
    setLoader(true);
    if (inpData.leaves && inpData.paidUnpaid && inpData.noOfLeaves > 0) {
      // console.log(inpData);
      client
        .mutate({
          mutation: CREATE_LEAVES,
          variables: {
            companyId: companyId,
            totalLeaves: inpData.noOfLeaves,
            type: inpData.leaves,
            isPaid: inpData.paidUnpaid,
          },
        })
        .then((res) => {
          // console.log(res);
          refetch();
          setInpData(() => {
            return { leaves: "", paidUnpaid: "", noOfLeaves: 0 };
          });
          Modal.success({
            content: "Leave added successfully",
          });
          setLoader(false);
        })
        .catch((error) => {
          console.log(error);
          Modal.error({
            title: "Leave can not be added",
          });
          setLoader(false);
        });
    } else {
      // console.log("error");
      Modal.error({
        title: "Invalid Data",
        content: "Input valid leaves, Paid/unpaid and no of leaves",
      });
      setLoader(false);
    }
  };

  const handleDelete = (props) => {
    // console.log(props);
    const leaveId = props;
    confirm({
      title: "Are you sure delete this leave?",
      okText: "Yes",
      okType: "danger",
      cancelText: "No",
      onOk() {
        client
          .mutate({
            mutation: DELETE_LEAVES,
            variables: { id: leaveId },
          })
          .then((res) => {
            refetch();
            Modal.success({
              content: "Leave Deleted successfully",
            });
          })
          .catch((error) => {
            console.log(error);
            Modal.error({
              content: "Leave can not be deleted",
            });
          });
      },
      onCancel() {},
    });
  };

  const handleCancel = () => {
    setInpData({
      leaves: "",
      noOfLeaves: "",
      paidUnpaid: "",
    });
  };

  return (
    <LeavesWrapper>
      <LeavesHeader>
        <div>
          <P fontsize="18px">Manage Leaves</P>
        </div>
        <div>{/* <UploadHolidayListButton /> */}</div>
      </LeavesHeader>
      <Divider />
      <LeavesList>
        <Table dataSource={tabledata} size="middle" pagination={false}>
          <Column
            title={<b>Leaves</b>}
            dataIndex="leaves"
            key="leaves"
            width="20%"
            className="table_left_border"
          />
          <Column
            title={<b>Paid/Unpaid</b>}
            dataIndex="paidUnpaid"
            key="paidUnpaid"
          />
          <Column
            title={<b>No of Leaves</b>}
            dataIndex="noOfLeaves"
            key="noOfLeaves"
            className={role && role !== "Admin" ? "table_right_border" : ""}
          />
          {role && role === "Admin" && (
            <Column
              title={<b>Actions</b>}
              dataIndex="actions"
              key="actions"
              className="table_right_border"
              width="8%"
              render={(text, record) => {
                if (record.key !== tabledata.length - 1) {
                  return (
                    <span>
                      <DeleteOutlined
                        onClick={() => handleDelete(record.key)}
                        style={{ padding: "0px 10px" }}
                      />
                    </span>
                  );
                } else {
                  return (
                    <UpdateBtn>
                      <Button
                        color="#FFFFFF"
                        bordercolor="#3C3C84"
                        bgcolor="#3C3C84"
                        onClick={handleSubmit}
                        loading={loader}
                        className="updtbtn"
                      >
                        Add
                      </Button>
                      <Button onClick={handleCancel}>Cancel</Button>
                    </UpdateBtn>
                  );
                }
              }}
            />
          )}
        </Table>
      </LeavesList>
    </LeavesWrapper>
  );
}

export default Leaves;
