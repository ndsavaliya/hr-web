import styled from "styled-components";

export const EmployeesWrapper = styled.div`
  max-height: 280px;
  overflow-y: auto;
  font-family: "Poppins", sans-serif;
  margin-top: 1%;
  width: 100%;

  &::-webkit-scrollbar {
    width: 3px;
  }

  &::-webkit-scrollbar-track {
    border-radius: 3px;
  }

  &::-webkit-scrollbar-thumb {
    background: #cccccc;
    border-radius: 2px;
  }
`;
