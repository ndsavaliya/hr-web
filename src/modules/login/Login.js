import React, { useState } from "react";
import {
  MainDiv,
  Wrapper,
  ImgText,
  LoginForm,
  ButtonLink,
  SignupLink,
} from "./css/LoginStyled";
import { P, Form, Input, Button, Modal } from "../AntStyles";
import { Link, withRouter } from "react-router-dom";
import { LOGIN } from "./graphql/Mutations";
import client from "../../apollo";

function LogInForm(props) {
  const [loader, setLoader] = useState(false);

  const handleFinish = values => {
    const { email, password } = values;
    setLoader(true);
    client
      .mutate({
        mutation: LOGIN,
        variables: { email, password },
      })
      .then(
        ({
          data: {
            login: { token },
          },
        }) => {
          localStorage.clear();
          localStorage.setItem("access_token", `Bearer ${token}`);
          props.history.push("/");
          Modal.success({
            content: "Login Successfully",
          });
        },
      )
      .catch(error => {
        console.log(error);
        Modal.error({
          title: "Invalid email and password",
          content: "Input valid email and password",
        });
        setLoader(false);
      });
  };

  return (
    <Form onFinish={handleFinish} layout="vertical">
      <Form.Item
        label="Email"
        name="email"
        rules={[
          {
            type: "email",
            message: "The input is not valid E-mail!",
          },
          {
            required: true,
            message: "Please input your E-mail!",
          },
        ]}
      >
        <Input size="large" />
      </Form.Item>

      <Form.Item
        label="Password"
        name="password"
        rules={[{ required: true, message: "Please input your password!" }]}
      >
        <Input type="password" size="large" />
      </Form.Item>

      <Form.Item>
        <Link to="/forgot-password">
          <P color="#999999" fontsize="12px" fontweight="500">
            Forgot password?
          </P>
        </Link>
      </Form.Item>

      <Form.Item>
        <ButtonLink>
          <Button
            className="loginbtn"
            color="#FFFFFF"
            bgcolor="#3C3C84"
            bordercolor="#3C3C84"
            htmlType="submit"
            loading={loader}
          >
            Login
          </Button>

          <SignupLink>
            <P color="#666666" fontsize="14px" fontweight="500">
              Don’t have account?
            </P>
            <Link to="/signup">
              <P color="#3C3C84" fontsize="14px" fontweight="500">
                <b>Signup</b>
              </P>
            </Link>
          </SignupLink>
        </ButtonLink>
      </Form.Item>
    </Form>
  );
}

function Login(props) {
  return (
    <MainDiv>
      <Wrapper>
        <ImgText>
          <div className="welcomemsg">
            <P color="#FFFFFF" fontsize="48px">
              Welcome Back!
            </P>
            <P color="#CCCCCC" fontsize="18px">
              To keep Connected with us
            </P>
            <P color="#CCCCCC" fontsize="18px">
              please login here with your details
            </P>
          </div>
        </ImgText>
        <LoginForm>
          <P color="#000000" fontsize="36px" fontweight="500">
            Login To HRS
          </P>
          <LogInForm {...props} />
        </LoginForm>
      </Wrapper>
    </MainDiv>
  );
}

export default withRouter(Login);
