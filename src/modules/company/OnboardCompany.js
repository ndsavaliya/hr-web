import React, { useContext } from "react";
import { TabBar, Avatar, P } from "../AntStyles";
import {
  CompanyWrapper,
  ProfileHeader,
  LogoNameLocation,
  NameLocationTimeZone,
  ProfilePic,
  ChangeProfile,
} from "./OnboardCompanyStyles";
import { Upload } from "antd";
import CompanyProfile from "./components/CompanyProfile";
import { startCase } from "lodash";
import { GetCompanyContext } from "../CompanyContextProvider";
import { GetUserContext } from "../UserContextProvider";
import WorkSchedule from "./components/WorkSchedule";
import Leaves from "./components/Leaves";

const { TabPane } = TabBar;
function OnboardCompany() {
  const { userData } = useContext(GetUserContext);
  const userdetails = userData || {};
  const { role = "" } = userdetails;
  const { companyData } = useContext(GetCompanyContext);
  const details = companyData || {};
  const { name = "", location = "", timezone = "" } = details;

  const logoSrc =
    "https://media-exp1.licdn.com/dms/image/C510BAQHRV8PqAHFOcQ/company-logo_200_200/0?e=2159024400&v=beta&t=XvC60fQt_qXIpw0XWT4gEkcBQDfKAsWHBKbldZQYSxE";

  return (
    <div>
      <CompanyWrapper>
        <ProfileHeader>
          <LogoNameLocation>
            <ProfilePic className="profilepic">
              <Avatar src={logoSrc} />
              {role === "Admin" && (
                <ChangeProfile>
                  <Upload>
                    <P color="#FFFFFF" ffontsize="12px">
                      Upload Company Logo
                    </P>
                  </Upload>
                </ChangeProfile>
              )}
            </ProfilePic>
            <NameLocationTimeZone>
              <P color="#3C3C84" fontsize="36px" fontweight="500">
                {name ? startCase(name) : "Not Defined"}
              </P>
              <P color="#666666" fontsize="18px">
                {location ? `${startCase(location)} Office` : "Not Defined"}
              </P>
              <P color="#666666" fontsize="14px" fontweight="300">
                {`Time zone in ${startCase(location)} (${timezone})`}
              </P>
            </NameLocationTimeZone>
          </LogoNameLocation>
          <TabBar defaultActiveKey="3">
            <TabPane tab="Company Profile" key="1">
              <CompanyProfile companyData={details} />
            </TabPane>
            <TabPane tab="Work Schedule" key="2">
              <WorkSchedule />
            </TabPane>
            <TabPane tab="Leaves" key="3">
              <Leaves />
            </TabPane>
            <TabPane tab="Emplyees" key="4">
              Employees
            </TabPane>
            <TabPane tab="Holidays" key="5">
              Holidays
            </TabPane>
            <TabPane tab="Rules" key="6">
              Rules
            </TabPane>
          </TabBar>
        </ProfileHeader>
      </CompanyWrapper>
    </div>
  );
}

export default OnboardCompany;
