import styled from "styled-components";

const flex = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
  font-family: "Poppins", sans-serif;
`;

export const DashboardWrapper = styled(flex)`
  flex-direction: column;
  padding: 10px;
`;

export const NoticeSearchWrapper = styled(flex)`
  justify-content: space-between;
  align-items: center;
`;

export const NoticeWrapper = styled.div`
  background-color: #f0f0fa;
  padding: 9px;
  border-radius: 4px;
  width: 90%;
  color: #3c3c84;
  margin-right: 2%;
`;

export const EmpLogin = styled.div`
  padding: 9px;
  border-radius: 4px;
  margin: 1% 0%;
`;
