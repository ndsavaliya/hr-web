import React from "react";
import { Divider, Avatar, P, Days } from "../../AntStyles";
import {
  LeaveSummaryWrapper,
  ScrollableLeaveSummaryDiv,
  LeaveDetails,
  ImgNameLeaves,
  Leaves,
} from "../css/LeaveSummaryStyles";
import { LISTING_LEAVES } from "../graphql/Queries";
import { useQuery } from "react-apollo";
import moment from "moment";
import { groupBy } from "lodash";
import { UserOutlined } from "@ant-design/icons";

function LeaveSummary() {
  const { loading, error, data } = useQuery(LISTING_LEAVES, {
    fetchPolicy: "network-only",
  });
  const leavesList = data && data.listingLeaveRequests;

  let upcomingLeaves = [];
  leavesList &&
    leavesList.length &&
    leavesList.map((leave, index) => {
      leave.leaves &&
        leave.leaves.length &&
        leave.leaves.map((_leave, key) => {
          const currentTime = `${moment().format("YYYY-MM-DD")}T00:00:00.000Z`;
          if (_leave.date >= currentTime) {
            upcomingLeaves.push({
              ..._leave,
              user: { ...leave.user },
              leaves: [...leave.leaves],
            });
          }
        });
    });

  const dateWiseLeave = groupBy(upcomingLeaves, function (leave) {
    return moment(leave.date).format("YYYY-MM-DD");
  });

  const dayArray = Object.keys(dateWiseLeave);

  return (
    <LeaveSummaryWrapper>
      <P color="#333333" fontsize="24px" fontweight="500">
        Leave Summary
      </P>
      <Divider />

      <ScrollableLeaveSummaryDiv>
        {loading
          ? "loading"
          : error
          ? "error"
          : dayArray && dayArray.length
          ? dayArray.map((day, index) => (
              <div key={index}>
                <P color="#666666" fontsize="18px" fontweight="500">
                  {moment(day).format("MMMM Do YYYY") ===
                  moment().format("MMMM Do YYYY")
                    ? "Today"
                    : moment(day).format("MMMM Do, YYYY")}
                </P>
                <Divider />

                {(dateWiseLeave[day] || []).map((_day, index) => (
                  <div key={index}>
                    <LeaveDetails>
                      <ImgNameLeaves>
                        {_day.user.profileImage ? (
                          <Avatar
                            marginright="10px"
                            size={50}
                            src={_day.user.profileImage}
                          />
                        ) : (
                          <Avatar
                            marginright="10px"
                            size={50}
                            icon={<UserOutlined />}
                          />
                        )}
                        <P color="#333333" fontsize="18px" fontweight="500">
                          {_day.user.name ? _day.user.name : _day.user.email}
                        </P>
                      </ImgNameLeaves>
                      <div>
                        <P color="#666666" fontsize="10px" fontweight="300">
                          {_day &&
                          _day.leaves &&
                          _day.leaves.length &&
                          _day.leaves.length === 1
                            ? `on ${moment(_day.leaves[0].date).format(
                                "DD/MM/YYYY",
                              )}`
                            : `from ${moment(_day.leaves[0].date).format(
                                "DD/MM/YYYY",
                              )} to ${moment(
                                _day.leaves[_day.leaves.length - 1].date,
                              ).format("DD/MM/YYYY")}`}
                        </P>
                        <Leaves>
                          {_day &&
                            _day.leaves &&
                            _day.leaves.length &&
                            _day.leaves.map((leaveday, index) => {
                              let leaveclass;
                              if (leaveday.type === "FULL_DAY") {
                                leaveclass = "full_leave";
                              } else if (leaveday.type === "FIRST_HALF") {
                                leaveclass = "firsthalf_leave";
                              } else if (leaveday.type === "SECOND_HALF") {
                                leaveclass = "sechalf_leave";
                              }
                              return (
                                <Days
                                  key={index}
                                  className={leaveclass}
                                  height_width="20px"
                                >
                                  <P
                                    color="#FFFFFF"
                                    fontsize="10px"
                                    fontweight="500"
                                  >
                                    {moment(leaveday.date).format("D")}
                                  </P>
                                </Days>
                              );
                            })}
                        </Leaves>
                      </div>
                    </LeaveDetails>
                    <Divider />
                  </div>
                ))}
              </div>
            ))
          : "No Leaves Data Found"}
      </ScrollableLeaveSummaryDiv>
    </LeaveSummaryWrapper>
  );
}

export default LeaveSummary;
