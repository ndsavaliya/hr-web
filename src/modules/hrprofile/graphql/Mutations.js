import gql from "graphql-tag";

export const CHANGE_PASSWORD = gql`
  mutation changePasswordMutation(
    $email: String!
    $password: String!
    $newPassword: String!
  ) {
    changePassword(
      email: $email
      password: $password
      newPassword: $newPassword
    ) {
      id
      employeeId
      name
      role
      email
      birthdate
      password
      companyId
      isDeleted
      emailVerified
      resetPasswordToken
      resetPasswordExpires
      registerToken
      registerTokenExpires
      joiningDate
      deletedAt
      punchStatus
      token
    }
  }
`;

export const IMPORT_USER_FILE = gql`
  mutation uploadUserFileMutation($file: Upload!) {
    uploadUserFile(file: $file)
  }
`;

export const REGISTER = gql`
  mutation registerMutation(
    $employeeId: String
    $name: String
    $email: String
    $role: String
    $birthdate: Date
    $joiningDate: Date
    $companyId: ID
  ) {
    register(
      input: {
        employeeId: $employeeId
        name: $name
        email: $email
        role: $role
        birthdate: $birthdate
        joiningDate: $joiningDate
        companyId: $companyId
      }
    ) {
      id
      name
      email
      companyId
    }
  }
`;

export const UPDATE_USER = gql`
  mutation updateUserMutation(
    $id: ID!
    $employeeId: String
    $name: String
    $profileImage: Upload
    $role: String
    $birthdate: Date
    $companyId: ID
    $joiningDate: Date
    $email: String
  ) {
    updateUser(
      where: { id: $id }
      input: {
        employeeId: $employeeId
        name: $name
        profileImage: $profileImage
        role: $role
        birthdate: $birthdate
        companyId: $companyId
        joiningDate: $joiningDate
        email: $email
      }
    ) {
      id
      employeeId
      name
      profileImage
      role
      email
      birthdate
      password
      companyId
      isDeleted
      emailVerified
      resetPasswordToken
      resetPasswordExpires
      registerToken
      registerTokenExpires
      joiningDate
      deletedAt
      punchStatus
      token
    }
  }
`;

export const DELETE_USER = gql`
  mutation deleteUserMutation($id: ID!) {
    deleteUser(id: $id) {
      id
      employeeId
      name
      role
      email
      birthdate
      password
      companyId
      isDeleted
      emailVerified
      resetPasswordToken
      resetPasswordExpires
      registerToken
      registerTokenExpires
      joiningDate
      deletedAt
      punchStatus
      token
    }
  }
`;

export const IMPORT_HOLIDAY_FILE = gql`
  mutation uploadHolidayFileMutation($file: Upload!) {
    uploadHolidayFile(file: $file)
  }
`;

export const CREATE_HOLIDAY = gql`
  mutation createHolidayMutation(
    $name: String!
    $date: Date!
    $companyId: ID!
  ) {
    createHoliday(input: { name: $name, date: $date, companyId: $companyId }) {
      id
      name
      date
      companyId
    }
  }
`;

export const UPDATE_HOLIDAY = gql`
  mutation updateHolidayMutation(
    $name: String!
    $date: Date!
    $companyId: ID!
    $id: ID!
  ) {
    updateHoliday(
      input: { name: $name, date: $date, companyId: $companyId }
      where: { id: $id }
    ) {
      id
      name
      date
      companyId
    }
  }
`;

export const DELETE_HOLIDAY = gql`
  mutation deleteHolidayMutation($id: ID!) {
    deleteHoliday(id: $id)
  }
`;
