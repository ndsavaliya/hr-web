import styled from "styled-components";

export const flex = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
  font-family: "Poppins", sans-serif;
`;

export const ScrollableDivApprovals = styled.div`
  margin-top: 10px;
  height: 310px;
  overflow-y: auto;
  padding-right: 10px;

  &::-webkit-scrollbar {
    width: 3px;
  }

  &::-webkit-scrollbar-track {
    border-radius: 3px;
  }

  &::-webkit-scrollbar-thumb {
    background: #cccccc;
    border-radius: 2px;
  }
`;

export const LeavesDetails = styled(flex)`
  justify-content: space-between;
  align-items: center;
  padding-right: 10px;
`;

export const ImgNameTime = styled(flex)`
  justify-content: stretch;
  align-items: center;
  margin: 0px 20px;
`;

export const LeaveDuration = styled(flex)`
  background-color: #f0f0fa;
  align-items: center;
  justify-content: center;
  color: #3c3c84;
  padding: 2% 5%;
  margin: 5% 5%;
  text-align: center;
`;

export const LeaveReason = styled.div`
  margin: 5% 5%;
  text-align: justify;
`;
