import { ApolloClient } from "apollo-boost";
import { createHttpLink } from "apollo-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";
import { ApolloLink, split } from "apollo-link";

import { WebSocketLink } from "apollo-link-ws";
import { getMainDefinition } from "apollo-utilities";
import { createUploadLink } from "apollo-upload-client";

const httpLink = createHttpLink({
  uri: "http://localhost:8080/graphql",
  credentials: "include",
});

const uploadLink = createUploadLink({
  headers: {
    authorization: localStorage.getItem("access_token"),
  },
  uri: "http://localhost:8080/graphql",
});

const wsLink = new WebSocketLink({
  uri: `ws://localhost:8080/graphql`,
  options: {
    reconnect: true,
    timeout: 30000,
    lazy: true,
    async connectionParams() {
      const authorizationToken = localStorage.getItem("access_token");
      return {
        authToken: authorizationToken ? authorizationToken : null,
      };
    },
  },
});

const link = split(
  ({ query }) => {
    const definition = getMainDefinition(query);
    return (
      definition.kind === "OperationDefinition" &&
      definition.operation === "subscription"
    );
  },
  wsLink,
  uploadLink,
  httpLink
);

const middlewareAuthLink = new ApolloLink((operation, forward) => {
  const token = localStorage.getItem("access_token");
  const authorizationHeader = token ? token : null;
  operation.setContext({
    headers: {
      authorization: authorizationHeader,
    },
  });
  return forward(operation);
});

const client = new ApolloClient({
  link: middlewareAuthLink.concat(link),
  cache: new InMemoryCache(),
  query: { fetchPolicy: "no-cache" },
});

export default client;
