import React, { useState, useEffect, useContext } from "react";
import {
  WorkScheduleWrapper,
  WorkDaySchedule,
  TimeFlexibility,
  WorkingHours,
  Timing,
  StartTime,
  EndTime,
  BreakFlexibility,
  ButtonDiv,
} from "../css/WorkScheduleStyles";
import { Divider, P, Radio, TimePicker, Button, Modal } from "../../AntStyles";
import { Col, Row } from "antd";
import moment from "moment";
import { DeleteOutlined, PlusOutlined } from "@ant-design/icons";
import { GET_PUNCH_SETTING } from "../graphql/Queries";
import { CREATE_PUNCH_SETTING } from "../graphql/Mutations";
import client from "../../../apollo";
import { GetUserContext } from "../../UserContextProvider";

function WorkSchedule() {
  const { userData } = useContext(GetUserContext);
  const details = userData || {};
  const { companyId = "" } = details;

  const [timeFlexibility, setTimeFlexibility] = useState(true);
  const [officeTime, setOfficeTime] = useState("YES");

  const [breakTimeFlexibility, setBreakTimeFlexibility] = useState(true);
  const [breakTime, setBreakTime] = useState("YES");

  const [inpData, setInpData] = useState({
    office: [{ startTime: "", endTime: "" }],
    break: [{ startTime: "", endTime: "" }],
  });

  const [punchSetting, setPunchSetting] = useState();
  const [loader, setLoader] = useState(false);

  useEffect(() => {
    if (companyId) {
      client
        .query({
          query: GET_PUNCH_SETTING,
          variables: { companyId },
        })
        .then((res) => {
          // console.log(res);
          setPunchSetting(res.data.getPunchSetting);
        })
        .catch((e) => {
          console.log(e);
        });
    }
  }, [companyId]);

  useEffect(() => {
    if (punchSetting) {
      // console.log(punchSetting);
      const {
        timeFlexibility,
        startTime,
        endTime,
        breakTimeFlexibility,
        breaks,
      } = punchSetting;
      setTimeFlexibility(timeFlexibility);
      setInpData((prevState) => {
        return {
          ...prevState,
          office: [{ startTime, endTime }],
          break: breaks,
        };
      });
      setBreakTimeFlexibility(breakTimeFlexibility);
      timeFlexibility ? setOfficeTime("YES") : setOfficeTime("NO");
      breakTimeFlexibility ? setBreakTime("YES") : setBreakTime("NO");
    }
  }, [punchSetting]);

  const onOfficeTimeChange = (e) => {
    const val = e.target.value;
    setOfficeTime(val);
    if (val === "NO") {
      setTimeFlexibility(false);
    } else {
      setTimeFlexibility(true);
    }
  };

  const onBreakTimeChange = (e) => {
    const val = e.target.value;
    setBreakTime(val);
    if (val === "NO") {
      setBreakTimeFlexibility(false);
    } else {
      setBreakTimeFlexibility(true);
    }
  };

  const handleBreakAdd = () => {
    const breakTimeArray = inpData.break;
    const values = [...breakTimeArray];
    values.push({ startTime: "", endTime: "" });
    setInpData((prevState) => {
      return { ...prevState, break: values };
    });
  };

  const handleBreakRemove = (i) => {
    const breakTimeArray = inpData.break;
    const values = [...breakTimeArray];
    values.splice(i, 1);
    setInpData((prevState) => {
      return { ...prevState, break: values };
    });
  };

  const handleBreakStartChange = (i, e) => {
    const breakTimeArray = inpData.break;
    const values = [...breakTimeArray];
    const time = e ? moment(e).format("hh:mm A") : "";
    values[i].startTime = time;
    setInpData((prevState) => {
      return { ...prevState, break: values };
    });
  };

  const handleBreakEndChange = (i, e) => {
    const breakTimeArray = inpData.break;
    const values = [...breakTimeArray];
    const time = e ? moment(e).format("hh:mm A") : "";
    values[i].endTime = time;
    setInpData((prevState) => {
      return { ...prevState, break: values };
    });
  };

  const handleSubmit = (e) => {
    // console.log(
    //   "before",
    //   timeFlexibility,
    //   breakTimeFlexibility,
    //   inpData.office[0],
    //   inpData.break,
    // );
    let setIsBreakTimeValid = false;
    let setIsOfficeTimeValid = false;
    let _startTime = null;
    let _endTime = null;
    let _break = [];

    if (timeFlexibility) {
      // console.log("timeFlexibility", timeFlexibility);
      const officeTimeArray = inpData.office;
      const values = [...officeTimeArray];
      values[0].startTime = "";
      values[0].endTime = "";
      setInpData((prevState) => {
        return { ...prevState, office: values };
      });
      setIsOfficeTimeValid = true;
    } else if (!timeFlexibility) {
      // console.log("timeFlexibility", timeFlexibility);
      const { startTime, endTime } = inpData.office[0];
      // console.log(startTime, endTime);
      if (
        !startTime ||
        !endTime ||
        startTime === "Invalid date" ||
        endTime === "Invalid date"
      ) {
        // console.log("if office if");
        Modal.error({
          title: "Enter valid start time & end time",
        });
        return (setIsOfficeTimeValid = false);
      } else {
        setIsOfficeTimeValid = true;
        _startTime = startTime;
        _endTime = endTime;
      }
    }

    if (breakTimeFlexibility) {
      // console.log("breakTimeFlexibility", breakTimeFlexibility);
      const val = [{ startTime: null, endTime: null }];
      _break = val;
      setIsBreakTimeValid = true;
      setInpData((prevState) => {
        return { ...prevState, break: val };
      });
    } else if (!breakTimeFlexibility) {
      // console.log("breakTimeFlexibility", breakTimeFlexibility);
      const data = inpData.break;
      for (let i in data) {
        if (
          !data[i].startTime ||
          !data[i].endTime ||
          data[i].startTime === "Invalid date" ||
          data[i].endTime === "Invalid date"
        ) {
          // console.log("if break if");
          Modal.error({
            title: "Enter valid start time & end time",
          });
          return (setIsBreakTimeValid = false);
        } else {
          setIsBreakTimeValid = true;
          _break = inpData.break;
        }
      }
    }

    // console.log(setIsOfficeTimeValid, setIsBreakTimeValid);
    if (setIsOfficeTimeValid && setIsBreakTimeValid) {
      // console.log(
      //   "after",
      //   timeFlexibility,
      //   _startTime,
      //   _endTime,
      //   breakTimeFlexibility,
      //   _break,
      // );

      client
        .mutate({
          mutation: CREATE_PUNCH_SETTING,
          variables: {
            companyId,
            timeFlexibility,
            startTime: _startTime,
            endTime: _endTime,
            breakTimeFlexibility,
            breaks: _break,
          },
        })
        .then((res) => {
          // console.log(res.data);
          const details = res.data.createPunchSetting;
          const {
            timeFlexibility,
            startTime,
            endTime,
            breakTimeFlexibility,
            breaks,
          } = details;
          setTimeFlexibility(timeFlexibility);
          setInpData((prevState) => {
            return {
              ...prevState,
              office: [{ startTime, endTime }],
              break: breaks,
            };
          });
          setBreakTimeFlexibility(breakTimeFlexibility);
          if (timeFlexibility) {
            setOfficeTime("YES");
          } else {
            setOfficeTime("NO");
          }
          if (breakTimeFlexibility) {
            setBreakTime("YES");
          } else {
            setBreakTime("NO");
          }
          Modal.success({
            content: "Working Schedule updated successfully",
          });
          setLoader(false);
        })
        .catch((error) => {
          console.log(error);
          Modal.error({
            title: "Working Schedule can not be updated",
          });
          const {
            timeFlexibility,
            startTime,
            endTime,
            breakTimeFlexibility,
            breaks,
          } = punchSetting;
          setTimeFlexibility(timeFlexibility);
          setInpData((prevState) => {
            return {
              ...prevState,
              office: [{ startTime, endTime }],
              break: breaks,
            };
          });
          setBreakTimeFlexibility(breakTimeFlexibility);
          if (timeFlexibility) {
            setOfficeTime("YES");
          } else {
            setOfficeTime("NO");
          }
          if (breakTimeFlexibility) {
            setBreakTime("YES");
          } else {
            setBreakTime("NO");
          }
          setLoader(false);
        });
    }
  };

  const handleCancel = () => {
    if (punchSetting) {
      // console.log(punchSetting);
      const {
        timeFlexibility,
        startTime,
        endTime,
        breakTimeFlexibility,
        breaks,
      } = punchSetting;
      setTimeFlexibility(timeFlexibility);
      setInpData((prevState) => {
        return {
          ...prevState,
          office: [{ startTime, endTime }],
          break: breaks,
        };
      });
      setBreakTimeFlexibility(breakTimeFlexibility);
      if (timeFlexibility) {
        setOfficeTime("YES");
      } else {
        setOfficeTime("NO");
      }
      if (breakTimeFlexibility) {
        setBreakTime("YES");
      } else {
        setBreakTime("NO");
      }
    }
  };

  return (
    <WorkScheduleWrapper>
      <div>
        <P color="#3C3C84" fontsize="24px" fontweight="500">
          Working Days Schedule
        </P>
      </div>
      <Divider />
      <WorkDaySchedule>
        {/* <Form> */}
        <TimeFlexibility>
          <P fontsize="18px">Do You Have Time Flexibility?</P>
          {/* <Form.Item name="office-time-radio-group"> */}
          <Radio.Group onChange={onOfficeTimeChange} value={officeTime}>
            <Row>
              <Col xs={6} lg={3}>
                <Radio value="YES">
                  <P color="#3C3C84" fontsize="18px">
                    Yes
                  </P>
                </Radio>
              </Col>
              <Col xs={6} lg={3}>
                <Radio value="NO">
                  <P color="#3C3C84" fontsize="18px">
                    No
                  </P>
                </Radio>
              </Col>
            </Row>
          </Radio.Group>
          {/* </Form.Item> */}
        </TimeFlexibility>
        {!timeFlexibility && (
          <WorkingHours>
            <P fontsize="18px">Specify Working Hours For A Day</P>
            <Timing>
              <StartTime>
                <P fontsize="16px">Start Time</P>
                <TimePicker
                  use12Hours
                  format="h:mm A"
                  value={
                    inpData.office[0].startTime
                      ? moment(inpData.office[0].startTime, "HH:mm:ss")
                      : ""
                  }
                  onChange={(e) => {
                    // console.log(e);
                    const officeTimeArray = inpData.office;
                    const values = [...officeTimeArray];
                    const time = e ? moment(e).format("hh:mm A") : "";
                    // console.log(time);
                    values[0].startTime = time;
                    setInpData((prevState) => {
                      return { ...prevState, office: values };
                    });
                  }}
                ></TimePicker>
              </StartTime>
              <EndTime>
                <P fontsize="16px">End Time</P>
                <TimePicker
                  use12Hours
                  format="h:mm A"
                  value={
                    inpData.office[0].endTime
                      ? moment(inpData.office[0].endTime, "HH:mm:ss")
                      : ""
                  }
                  onChange={(e) => {
                    const officeTimeArray = inpData.office;
                    const values = [...officeTimeArray];
                    const time = e ? moment(e).format("hh:mm A") : "";
                    // console.log(time);
                    values[0].endTime = time;
                    setInpData((prevState) => {
                      return { ...prevState, office: values };
                    });
                  }}
                ></TimePicker>
              </EndTime>
            </Timing>
          </WorkingHours>
        )}
        <BreakFlexibility>
          <P fontsize="18px">Do You Have Flexible Break Timing?</P>
          {/* <Form.Item name="break-time-radio-group"> */}
          <Radio.Group onChange={onBreakTimeChange} value={breakTime}>
            <Row>
              <Col xs={6} lg={3}>
                <Radio value="YES">
                  <P color="#3C3C84" fontsize="18px">
                    Yes
                  </P>
                </Radio>
              </Col>
              <Col xs={6} lg={3}>
                <Radio value="NO">
                  <P color="#3C3C84" fontsize="18px">
                    No
                  </P>
                </Radio>
              </Col>
            </Row>
          </Radio.Group>
          {/* </Form.Item> */}
        </BreakFlexibility>
        {!breakTimeFlexibility && (
          <WorkingHours>
            <P fontsize="18px">
              How Many Breaks Do You Have And Add Timing As Well?
            </P>
            {inpData && inpData.break && inpData.break.length
              ? inpData.break.map((field, idx) => {
                  // console.log(field);
                  return (
                    <Timing key={idx}>
                      <P fontsize="18px">{idx + 1}</P>
                      <StartTime>
                        <P fontsize="16px">Start Time</P>
                        <TimePicker
                          use12Hours
                          format="h:mm A"
                          // value={field.startTime}
                          value={
                            field.startTime
                              ? moment(field.startTime, "h:mm A")
                              : ""
                          }
                          onChange={(e) => handleBreakStartChange(idx, e)}
                        ></TimePicker>
                      </StartTime>
                      <EndTime>
                        <P fontsize="16px">End Time</P>
                        <TimePicker
                          use12Hours
                          format="h:mm A"
                          // value={inpData.break.endTime}
                          value={
                            field.endTime ? moment(field.endTime, "h:mm A") : ""
                          }
                          onChange={(e) => handleBreakEndChange(idx, e)}
                        ></TimePicker>
                      </EndTime>
                      <div>
                        {idx === 0 ? (
                          <PlusOutlined
                            onClick={() => handleBreakAdd()}
                            style={{ paddingTop: "32px" }}
                          />
                        ) : (
                          <DeleteOutlined
                            onClick={() => handleBreakRemove(idx)}
                            style={{ paddingTop: "32px" }}
                          />
                        )}
                      </div>
                    </Timing>
                  );
                })
              : ""}
          </WorkingHours>
        )}
        <ButtonDiv>
          <Button
            className="cancelbtn"
            color="#FFFFFF"
            bgcolor="#999999"
            bordercolor="#999999"
            loading={loader}
            onClick={handleCancel}
          >
            CANCEL
          </Button>
          <Button
            color="#FFFFFF"
            bordercolor="#3C3C84"
            bgcolor="#3C3C84"
            // loading={reqLeaveLoader}
            onClick={handleSubmit}
          >
            SAVE
          </Button>
        </ButtonDiv>
        {/* </Form> */}
      </WorkDaySchedule>
    </WorkScheduleWrapper>
  );
}

export default WorkSchedule;
