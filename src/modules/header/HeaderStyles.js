import styled from "styled-components";

const flex = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
  font-family: "Poppins", sans-serif;
`;

export const HeaderWrapper = styled(flex)`
  justify-content: space-between;
  align-items: center;
  background-color: #3c3c84;
  padding: 1%;
`;

export const HeaderTwo = styled(flex)`
  align-items: center;
  padding: 0 1%;
`;

export const HeaderDiv = styled.div`
  padding-right: 20px;
  color: #ffffff;
`;

export const ProfileDiv = styled(flex)`
  align-items: center;
  padding-right: 20px;
  color: #ffffff;
`;
