import React, { useState, useContext, useEffect } from "react";
import Editor from "./Editor";
import { LeaveType, DateTime } from "../css/LeaveRequestsStyles";
import { Button, Select, DatePicker, P, Modal, Form } from "../../AntStyles";
import {
  ALL_USERS,
  GET_ALL_LEAVE_REQUEST,
  LEAVE_SETTING,
} from "../graphql/Queries";
import { CREATE_LEAVE_REQUEST } from "../graphql/Mutations";
import { useQuery } from "react-apollo";
import { DeleteOutlined, PlusOutlined } from "@ant-design/icons";
import moment from "moment";
import { startCase } from "lodash";
import client from "../../../apollo";
import { GetUserContext } from "../../UserContextProvider";

const { Option } = Select;

function RequestLeaveModel() {
  const { userData } = useContext(GetUserContext);
  const details = userData || {};
  const { id = "", companyId = "" } = details;

  const [loader, setLoader] = useState(false);
  const [reqLeaveLoader, setReqLeaveLoader] = useState(false);
  const [reqVisible, setReqVisible] = useState(false);
  const [leaveTypeData, setLeaveTypeData] = useState([]);
  const [inpData, setInpData] = useState({
    leaveType: "",
    dateTime: [{ date: moment(), type: "" }],
    sendTo: [],
    reasonDetails: "",
  });

  useEffect(() => {
    if (companyId) {
      client
        .query({
          query: LEAVE_SETTING,
          variables: { companyId },
        })
        .then((res) => {
          setLeaveTypeData(res.data.getLeaveSetting);
        })
        .catch((e) => {
          console.log(e);
        });
    }
  }, [companyId]);

  const { loading, error, data } = useQuery(ALL_USERS, {
    fetchPolicy: "network-only",
  });
  const allUsersData = data;

  const showModalReq = () => {
    setReqLeaveLoader(true);
    setReqVisible(true);
  };

  const handleReqOk = (e) => {
    const { leaveType, dateTime, sendTo, reasonDetails } = inpData;
    if (leaveType && dateTime && dateTime.length && sendTo && sendTo.length) {
      setLoader(true);
      client
        .mutate({
          mutation: CREATE_LEAVE_REQUEST,
          variables: {
            requestType: leaveType,
            leaves: dateTime,
            assignment: sendTo,
            description: reasonDetails,
          },
          refetchQueries: [
            {
              query: GET_ALL_LEAVE_REQUEST,
              variables: { userId: id },
              fetchPolicy: "network-only",
            },
          ],
        })
        .then((res) => {
          Modal.success({
            content: "Leave requested successfully",
          });
          setLoader(false);
          setReqLeaveLoader(false);
          setInpData({
            leaveType: "",
            dateTime: [{ date: moment(), type: "" }],
            sendTo: [],
            reasonDetails: "",
          });
          setReqVisible(false);
        })
        .catch((error) => {
          console.log(error);
          Modal.error({
            title: "Incorrect Data",
          });
          setLoader(false);
        });
    } else {
      Modal.error({
        title: "Input Leave type, date, time & send to",
      });
    }
  };

  const handleReqCancel = (e) => {
    setInpData({
      leaveType: "",
      dateTime: [{ date: moment(), type: "" }],
      sendTo: [],
      reasonDetails: "",
    });
    setReqVisible(false);
    setReqLeaveLoader(false);
  };

  const handleAdd = () => {
    const dateTimeArray = inpData.dateTime;
    const values = [...dateTimeArray];
    values.push({ date: moment(), type: "" });
    setInpData((prevState) => {
      return { ...prevState, dateTime: values };
    });
  };

  const handleRemove = (i) => {
    const dateTimeArray = inpData.dateTime;
    const values = [...dateTimeArray];
    values.splice(i, 1);
    setInpData((prevState) => {
      return { ...prevState, dateTime: values };
    });
  };

  const handleDateChange = (i, e) => {
    const dateTimeArray = inpData.dateTime;
    const values = [...dateTimeArray];

    values[i].date = `${moment(e).format("YYYY-MM-DD")}T00:00:00.000Z`;
    setInpData((prevState) => {
      return { ...prevState, dateTime: values };
    });
  };

  const handleTimeChange = (i, e) => {
    const dateTimeArray = inpData.dateTime;
    const values = [...dateTimeArray];
    values[i].type = e;
    setInpData((prevState) => {
      return { ...prevState, dateTime: values };
    });
  };

  const handleEditorChange = (props) => {
    let text = props;
    setInpData((prevState) => {
      return { ...prevState, reasonDetails: text };
    });
  };

  return (
    <div>
      <Button
        color="#FFFFFF"
        bordercolor="#3C3C84"
        bgcolor="#3C3C84"
        loading={reqLeaveLoader}
        onClick={showModalReq}
      >
        REQUEST LEAVE
      </Button>
      <Modal
        className="request"
        title="Leave Request"
        visible={reqVisible}
        onApprove={handleReqOk}
        onCancel={handleReqCancel}
        footer={[
          <Button
            key="reject"
            color="#FFFFFF"
            bordercolor="#999999"
            bgcolor="#999999"
            onClick={handleReqCancel}
          >
            CANCEL
          </Button>,
          <Button
            key="approve"
            color="#FFFFFF"
            bordercolor="#3C3C84"
            bgcolor="#3C3C84"
            onClick={handleReqOk}
            loading={loader}
          >
            SEND
          </Button>,
        ]}
      >
        <div>
          <Form>
            <LeaveType>
              <P color="#666666" fontsize="12px">
                Leave Type
              </P>
              <Select
                size="large"
                className="leave_type"
                value={inpData.leaveType}
                onChange={(e) => {
                  const val = e;
                  setInpData((prevState) => {
                    return { ...prevState, leaveType: val };
                  });
                }}
              >
                {(leaveTypeData || []).flatMap((leaves) =>
                  leaves
                    ? (leaves.leaveTypes || []).map((_leaveType) => (
                        <Option key={_leaveType.id} value={_leaveType.type}>
                          {startCase(_leaveType.type)}
                        </Option>
                      ))
                    : [],
                )}
              </Select>
            </LeaveType>
            {inpData && inpData.dateTime && inpData.dateTime.length
              ? inpData.dateTime.map((field, idx) => {
                  return (
                    <DateTime key={`${field}-${idx}`}>
                      <div>
                        <P color="#666666" fontsize="12px">
                          Date
                        </P>
                        <DatePicker
                          className="datepicker_class"
                          size="large"
                          format="DD/MM/YYYY"
                          value={inpData.dateTime.date}
                          onChange={(e) => handleDateChange(idx, e)}
                        />
                      </div>
                      <div>
                        <P color="#666666" fontsize="12px">
                          Time
                        </P>
                        <Select
                          size="large"
                          className="leave_time"
                          value={inpData.dateTime.type}
                          onChange={(e) => handleTimeChange(idx, e)}
                        >
                          <Option value="FULL_DAY">Full Day</Option>
                          <Option value="FIRST_HALF">First Half </Option>
                          <Option value="SECOND_HALF">Second Half</Option>
                        </Select>
                      </div>
                      <div>
                        {idx === 0 ? (
                          <PlusOutlined
                            onClick={() => handleAdd()}
                            style={{ paddingTop: "32px" }}
                          />
                        ) : (
                          <DeleteOutlined
                            onClick={() => handleRemove(idx)}
                            style={{ paddingTop: "32px" }}
                          />
                        )}
                      </div>
                    </DateTime>
                  );
                })
              : ""}
            <LeaveType>
              <P color="#666666" fontsize="12px">
                Send To
              </P>
              <Select
                mode="tags"
                size="large"
                tokenSeparators={[","]}
                className="leave_type"
                value={inpData.sendTo}
                onChange={(e) => {
                  const val = e;
                  console.log(e);
                  setInpData((prevState) => {
                    return { ...prevState, sendTo: val };
                  });
                }}
              >
                {loading ? (
                  <Option>Loading</Option>
                ) : error ? (
                  <Option>error</Option>
                ) : (
                  allUsersData &&
                  allUsersData.allUsers &&
                  allUsersData.allUsers.map((user, key) => (
                    <Option value={user.id} key={key}>
                      {user.email ? user.email : user.name}
                    </Option>
                  ))
                )}
              </Select>
            </LeaveType>

            <LeaveType>
              <P color="#333333" fontsize="18px">
                Reason Details
              </P>
              <Editor
                editorHtml={inpData.reasonDetails}
                handleChange={handleEditorChange}
              />
            </LeaveType>
          </Form>
        </div>
      </Modal>
    </div>
  );
}

export default RequestLeaveModel;
