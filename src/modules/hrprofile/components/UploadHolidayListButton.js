import React, { useEffect, useContext } from "react";
import { useState } from "react";
import { Modal, Button } from "../../AntStyles";
import { Upload, Tooltip } from "antd";
import { IMPORT_HOLIDAY_FILE } from "../graphql/Mutations";
import { ALL_HOLIDAYS } from "../graphql/Queries";
import client from "../../../apollo";
import { GetUserContext } from "../../UserContextProvider";

function UploadHolidayListButton() {
  const { userData } = useContext(GetUserContext);
  const details = userData || {};
  const { companyId = "", role = "" } = details;
  const [fileList, setFileList] = useState([]);

  useEffect(() => {
    if (fileList && fileList.length) {
      const file = fileList[0];
      client
        .mutate({
          mutation: IMPORT_HOLIDAY_FILE,
          variables: {
            file,
          },
          refetchQueries: [
            {
              query: ALL_HOLIDAYS,
              variables: { companyId },
              fetchPolicy: "network-only",
            },
          ],
        })
        .then((res) => {
          Modal.success({
            content: "File uploaded successfully",
          });
        })
        .catch((error) => {
          console.log(error);
          Modal.error({
            title: "File can't uploaded",
          });
        });
    }
  }, [companyId, fileList]);

  const handleBeforeUpload = (file) => {
    let fileExt = file.name.split(".");
    fileExt = fileExt[fileExt.length - 1];

    if (
      file.type === "application/vnd.ms-excel" &&
      fileExt.toLowerCase() === "csv"
    ) {
      let fileListData = [file];
      setFileList(fileListData);
      return false;
    } else {
      Modal.error({
        title: "Uploaded file should be in type of .csv",
      });
    }
  };

  return (
    <Upload showUploadList={false} beforeUpload={handleBeforeUpload}>
      <Tooltip placement="topRight" title="Import .csv file format only">
        <Button
          color="#FFFFFF"
          bordercolor="#3C3C84"
          bgcolor="#3C3C84"
          disabled={role && role === "Admin" ? false : true}
        >
          IMPORT HOLIDAY LIST
        </Button>
      </Tooltip>
    </Upload>
  );
}

export default UploadHolidayListButton;
