import gql from "graphql-tag";

export const UPDATE_COMPANY = gql`
  mutation updateCompanyMutation(
    $id: ID
    $name: String!
    $location: String!
    $timezone: String!
  ) {
    updateCompany(
      where: { id: $id }
      input: { name: $name, location: $location, timezone: $timezone }
    ) {
      id
      name
      location
      timezone
    }
  }
`;

export const CREATE_PUNCH_SETTING = gql`
  mutation createPunchSettingMutation(
    $companyId: ID
    $timeFlexibility: Boolean
    $startTime: DateTime
    $endTime: DateTime
    $breakTimeFlexibility: Boolean
    $breaks: [JSON]
  ) {
    createPunchSetting(
      input: {
        companyId: $companyId
        timeFlexibility: $timeFlexibility
        startTime: $startTime
        endTime: $endTime
        breakTimeFlexibility: $breakTimeFlexibility
        breaks: $breaks
      }
    ) {
      id
      companyId
      timeFlexibility
      startTime
      endTime
      breakTimeFlexibility
      breaks
    }
  }
`;

export const CREATE_LEAVES = gql`
  mutation createLeaveSettingMutation(
    $companyId: ID
    $type: String
    $totalLeaves: ID
    $isPaid: String
  ) {
    createLeaveSetting(
      input: {
        companyId: $companyId
        leaveTypes: [
          { totalLeaves: $totalLeaves, type: $type, isPaid: $isPaid }
        ]
      }
    ) {
      id
      companyId
      leaveTypes {
        id
        leaveSettingId
        type
        totalLeaves
        isPaid
      }
    }
  }
`;

export const DELETE_LEAVES = gql`
  mutation deleteLeaveType($id: ID!) {
    deleteLeaveType(id: $id)
  }
`;
