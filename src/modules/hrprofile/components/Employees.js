import React, { useState, useEffect, useContext } from "react";
import {
  Avatar,
  Input,
  DatePicker,
  Select,
  P,
  Button,
  Divider,
  Table,
  Modal,
} from "../../AntStyles";
import {
  EmployeesWrapper,
  EmployeesHeader,
  EmployeesList,
  UpdateBtn,
} from "../css/EmployeesStyles";
import moment from "moment";
import { REGISTER, UPDATE_USER, DELETE_USER } from "../graphql/Mutations";
import { ALL_USERS, GET_USER } from "../graphql/Queries";
import { useQuery } from "react-apollo";
import client from "../../../apollo";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { GetUserContext } from "../../UserContextProvider";
import UploadEmployeeListButton from "./UploadEmployeeListButton";
import { UserOutlined } from "@ant-design/icons";

const { Column } = Table;
const { Option } = Select;
const { confirm } = Modal;

function Employees() {
  const { userData } = useContext(GetUserContext);
  const details = userData || {};
  const { companyId = "", role = "" } = details;

  const [loader, setLoader] = useState(false);
  const [editable, setEditable] = useState(false);
  const [userId, setUserId] = useState("");
  const [inpData, setInpData] = useState({
    employeeId: "",
    name: "",
    email: "",
    role: "",
    birthDate: moment(),
    joiningDate: moment(),
    isEditable: true,
  });

  useEffect(() => {
    if (userId) {
      client
        .query({
          query: GET_USER,
          variables: { id: userId },
        })
        .then((res) => {
          const details = res.data.getUser;
          const { employeeId, name, email, role } = details;
          const birthDate = details.birthdate
            ? moment(details.birthdate)
            : moment();
          const joiningDate = details.joiningDate
            ? moment(details.joiningDate)
            : moment();
          setInpData(() => {
            return {
              employeeId,
              name,
              email,
              role,
              birthDate,
              joiningDate,
              isEditable: false,
            };
          });
        })
        .catch((error) => {
          console.error(error);
        });
    }
  }, [userId]);

  const { loading, error, data, refetch } = useQuery(ALL_USERS, {
    fetchPolicy: "network-only",
  });

  if (loading) return <div>loading</div>;
  if (error) return <div>error</div>;
  const allUsersList = data.allUsers;
  let rowdata = [];

  rowdata =
    allUsersList && allUsersList.length
      ? allUsersList.map((user, index) => {
          return userId === user.id && editable
            ? {
                key: user.id,
                employeeId: (
                  <Input
                    size="large"
                    value={inpData.employeeId}
                    onChange={(e) => {
                      const val = e.target.value;
                      setInpData((prevState) => {
                        return { ...prevState, employeeId: val };
                      });
                    }}
                  />
                ),
                profile: user.profileImage ? (
                  <Avatar size="small" src={user.profileImage} />
                ) : (
                  <Avatar size="small" icon={<UserOutlined />} />
                ),
                name: (
                  <Input
                    className="input80"
                    value={inpData.name}
                    onChange={(e) => {
                      const val = e.target.value;
                      setInpData((prevState) => {
                        return { ...prevState, name: val };
                      });
                    }}
                  />
                ),
                email: (
                  <Input
                    size="large"
                    value={inpData.email}
                    onChange={(e) => {
                      const val = e.target.value;
                      setInpData((prevState) => {
                        return { ...prevState, email: val };
                      });
                    }}
                    disabled
                  />
                ),
                jobPost: (
                  <Select
                    size="large"
                    className="selectBorderRadius"
                    value={inpData.role}
                    onChange={(e) => {
                      const val = e;
                      setInpData((prevState) => {
                        return { ...prevState, role: val };
                      });
                    }}
                  >
                    <Option value="Manager">Manager</Option>
                    <Option value="Employee">Employee</Option>
                  </Select>
                ),
                birthDate: (
                  <DatePicker
                    size="large"
                    className="dateBorderRadius"
                    format="DD/MM/YYYY"
                    value={inpData && inpData.birthDate}
                    disabledDate={(current) => current && current > moment()}
                    onChange={(e) => {
                      const val = e;
                      setInpData((prevState) => {
                        return { ...prevState, birthDate: val };
                      });
                    }}
                  />
                ),
                joiningDate: (
                  <DatePicker
                    size="large"
                    className="dateBorderRadius"
                    format="DD/MM/YYYY"
                    value={inpData && inpData.joiningDate}
                    disabledDate={(current) => current && current > moment()}
                    onChange={(e) => {
                      const val = e;
                      setInpData((prevState) => {
                        return { ...prevState, joiningDate: val };
                      });
                    }}
                  />
                ),
              }
            : {
                key: user.id,
                employeeId: user.employeeId ? user.employeeId : "",
                profile: user.profileImage ? (
                  <Avatar size="small" src={user.profileImage} />
                ) : (
                  <Avatar size="small" icon={<UserOutlined />} />
                ),
                name: user.name ? user.name : "",
                email: user.email ? user.email : "",
                jobPost: user.role ? user.role : "",
                birthDate: user.birthdate
                  ? moment(user.birthdate).format("DD/MM/YYYY")
                  : "",
                joiningDate: user.joiningDate
                  ? moment(user.joiningDate).format("DD/MM/YYYY")
                  : "",
                isEditable: false,
              };
        })
      : "";

  let tabledata = [];
  if (role === "Admin") {
    tabledata = [
      ...rowdata,
      {
        key: rowdata.length,
        employeeId: (
          <Input
            size="large"
            value={!editable ? inpData.employeeId : ""}
            onChange={(e) => {
              const val = e.target.value;
              setInpData((prevState) => {
                return { ...prevState, employeeId: val };
              });
            }}
          />
        ),
        profile: <Avatar marginright="10px" size="small" />,
        name: (
          <Input
            className="input80"
            value={!editable ? inpData.name : ""}
            onChange={(e) => {
              const val = e.target.value;
              setInpData((prevState) => {
                return { ...prevState, name: val };
              });
            }}
          />
        ),
        email: (
          <Input
            type="email"
            size="large"
            value={!editable ? inpData.email : ""}
            onChange={(e) => {
              const val = e.target.value;
              setInpData((prevState) => {
                return { ...prevState, email: val };
              });
            }}
          />
        ),
        jobPost: (
          <Select
            size="large"
            className="selectBorderRadius"
            value={!editable ? inpData.role : ""}
            onChange={(e) => {
              const val = e;
              setInpData((prevState) => {
                return { ...prevState, role: val };
              });
            }}
          >
            <Option value="Manager">Manager</Option>
            <Option value="Employee">Employee</Option>
          </Select>
        ),
        birthDate: (
          <DatePicker
            size="large"
            className="dateBorderRadius"
            format="DD/MM/YYYY"
            value={!editable ? inpData.birthDate : ""}
            disabledDate={(current) => current && current > moment()}
            onChange={(e) => {
              const val = e;
              setInpData((prevState) => {
                return { ...prevState, birthDate: val };
              });
            }}
          />
        ),
        joiningDate: (
          <DatePicker
            size="large"
            className="dateBorderRadius"
            format="DD/MM/YYYY"
            value={!editable ? inpData.joiningDate : ""}
            disabledDate={(current) => current && current > moment()}
            onChange={(e) => {
              const val = e;
              setInpData((prevState) => {
                return { ...prevState, joiningDate: val };
              });
            }}
          />
        ),
        isEditable: true,
      },
    ];
  } else {
    tabledata = [...rowdata];
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    setLoader(true);
    let { employeeId, name, email, role, birthDate, joiningDate } = inpData;
    if (name && email) {
      let bDate = `${moment(birthDate).format("YYYY-MM-DD")}T00:00:00.000Z`;
      let jDate = `${moment(joiningDate).format("YYYY-MM-DD")}T00:00:00.000Z`;

      client
        .mutate({
          mutation: REGISTER,
          variables: {
            employeeId,
            name,
            email,
            role,
            birthdate: bDate,
            joiningDate: jDate,
            companyId,
          },
        })
        .then((res) => {
          refetch();
          setInpData(() => {
            return {
              employeeId: "",
              profile: "",
              name: "",
              email: "",
              role: "",
              birthDate: moment(),
              joiningDate: moment(),
            };
          });
          Modal.success({
            content: "Check User's email for set a password",
          });
          setLoader(false);
        })
        .catch((error) => {
          console.log(error);
          Modal.error({
            title: "Incorrect Data",
          });
          setLoader(false);
        });
    } else {
      Modal.error({
        title: "Invalid Data",
        content: "Input valid Data",
      });
      setLoader(false);
    }
  };

  const handleDelete = (props) => {
    const uId = props;
    confirm({
      title: "Are you sure delete this User?",
      okText: "Yes",
      okType: "danger",
      cancelText: "No",
      onOk() {
        client
          .mutate({
            mutation: DELETE_USER,
            variables: { id: uId },
          })
          .then((res) => {
            refetch();
            Modal.success({
              content: "User Deleted successfully",
            });
          })
          .catch((error) => {
            console.log(error);
            Modal.error({
              content: "User can not be deleted",
            });
          });
      },
      onCancel() {},
    });
  };

  const handleEdit = (props) => {
    setUserId(props);
    setEditable(true);
  };

  const handleUpdate = () => {
    const { employeeId, name, email, role, birthDate, joiningDate } = inpData;
    setLoader(true);
    if (
      employeeId &&
      name &&
      email &&
      role &&
      birthDate &&
      joiningDate &&
      moment(birthDate).format("DD/MM/YYYY") !== moment().format("DD/MM/YYYY")
    ) {
      client
        .mutate({
          mutation: UPDATE_USER,
          variables: {
            id: userId,
            employeeId,
            name,
            role,
            birthdate: birthDate,
            companyId,
            joiningDate,
            email,
          },
        })
        .then((res) => {
          refetch();
          setEditable(false);
          Modal.success({
            content: "User Updated successfully",
          });
          setLoader(false);
          setInpData(() => {
            return {
              employeeId: "",
              profile: "",
              name: "",
              email: "",
              role: "",
              birthDate: moment(),
              joiningDate: moment(),
            };
          });
        })
        .catch((error) => {
          console.log(error);
          Modal.error({
            content: "User can not be updated",
          });
          setLoader(false);
        });
    } else {
      Modal.error({
        title: "Invalid Data",
      });
    }
  };

  const handleUpdateCancel = () => {
    setUserId("");
    setInpData(() => {
      return {
        employeeId: "",
        profile: "",
        name: "",
        email: "",
        role: "",
        birthDate: moment(),
        joiningDate: moment(),
      };
    });
    setEditable(false);
    setLoader(false);
  };

  const handleCancel = () => {
    setLoader(false);
    setInpData({
      employeeId: "",
      name: "",
      email: "",
      role: "",
      birthDate: moment(),
      joiningDate: moment(),
    });
  };

  return (
    <EmployeesWrapper>
      <EmployeesHeader>
        <div>
          <P fontsize="18px">Manage Employees</P>
        </div>
        <div>
          <UploadEmployeeListButton />
        </div>
      </EmployeesHeader>
      <Divider />
      <EmployeesList>
        <Table
          dataSource={tabledata}
          className="tableemp"
          size="middle"
          pagination={false}
        >
          <Column
            title={<b>Employee Id</b>}
            dataIndex="employeeId"
            key="employeeId"
            width="10%"
            className="table_left_border"
          />
          <Column dataIndex="profile" key="profile" />
          <Column title={<b>Name</b>} dataIndex="name" key="name" />
          <Column title={<b>Email</b>} dataIndex="email" key="email" />
          <Column
            title={<b>Job Post</b>}
            dataIndex="jobPost"
            key="jobPost"
            sorter={(a, b) => {
              if (a.isEditable) {
                return false;
              }
              return a.jobPost > b.jobPost ? 1 : -1;
            }}
            sortDirections={["descend"]}
            width="10%"
          />
          <Column
            title={<b>Birth Date</b>}
            dataIndex="birthDate"
            key="birthDate"
            width="10%"
          />
          <Column
            title={<b>Joining Date</b>}
            dataIndex="joiningDate"
            key="joiningDate"
            className={role && role !== "Admin" ? "table_right_border" : ""}
            sorter={(a, b) => {
              if (a.isEditable) {
                return false;
              }
              return (
                moment(a.joiningDate, "DD/MM/YYYY").unix() -
                moment(b.joiningDate, "DD/MM/YYYY").unix()
              );
            }}
            sortDirections={["descend", "ascend"]}
            width="10%"
          />
          {role && role === "Admin" && (
            <Column
              title={<b>Action</b>}
              key="action"
              className="table_right_border"
              render={(text, record) => {
                if (record.key !== tabledata.length - 1) {
                  return userId === record.key && editable ? (
                    <UpdateBtn>
                      <Button
                        className="updtbtn"
                        color="#FFFFFF"
                        bordercolor="#3C3C84"
                        bgcolor="#3C3C84"
                        onClick={handleUpdate}
                        loading={loader}
                      >
                        Update
                      </Button>
                      <Button onClick={handleUpdateCancel}>Cancel</Button>
                    </UpdateBtn>
                  ) : (
                    <span>
                      <DeleteOutlined
                        onClick={() => handleDelete(record.key)}
                        style={{ padding: "0px 10px" }}
                      />
                      <EditOutlined
                        onClick={() => handleEdit(record.key)}
                        style={{ padding: "0px 10px" }}
                      />
                    </span>
                  );
                } else {
                  return (
                    <UpdateBtn>
                      <Button
                        color="#FFFFFF"
                        bordercolor="#3C3C84"
                        bgcolor="#3C3C84"
                        onClick={handleSubmit}
                        loading={loader}
                        className="updtbtn"
                      >
                        Add
                      </Button>
                      <Button onClick={handleCancel}>Cancel</Button>
                    </UpdateBtn>
                  );
                }
              }}
            />
          )}
        </Table>
      </EmployeesList>
    </EmployeesWrapper>
  );
}

export default Employees;
