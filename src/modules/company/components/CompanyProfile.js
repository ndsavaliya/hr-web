import React, { useState, useContext, useEffect } from "react";
import { Input, Button, Avatar, P, Select, Form, Modal } from "../../AntStyles";
import { Row, Col, Upload } from "antd";
import {
  CompanyProfileWrapper,
  ProfilePic,
  ChangeProfile,
  ButtonDiv,
} from "../css/CompanyProfileStyles";
import { UPDATE_COMPANY } from "../graphql/Mutations";
import client from "../../../apollo";
import { GetCompanyContext } from "../../CompanyContextProvider";
import moment from "moment-timezone";
import { GetUserContext } from "../../UserContextProvider";

const { Option } = Select;

const logoSrc =
  "https://media-exp1.licdn.com/dms/image/C510BAQHRV8PqAHFOcQ/company-logo_200_200/0?e=2159024400&v=beta&t=XvC60fQt_qXIpw0XWT4gEkcBQDfKAsWHBKbldZQYSxE";

function CompanyForm(props) {
  const { userData } = useContext(GetUserContext);
  const userdetails = userData || {};
  const { role = "" } = userdetails;
  const [form] = Form.useForm();
  const { companyData, setCompanyData } = useContext(GetCompanyContext);
  const details = companyData || {};
  const { id, name, location, timezone } = details;
  const [loader, setLoader] = useState(false);
  const timeZones = moment.tz.names();
  let offsetTmz = [];

  for (let i in timeZones) {
    offsetTmz.push(
      `(GMT${moment.tz(timeZones[i]).format("Z")}) ${timeZones[i]}`,
    );
  }
  const timeZoneNames = offsetTmz.sort();

  useEffect(() => {
    if (props) {
      setCompanyData(props.companyData);
    }
  }, [props, setCompanyData]);

  useEffect(() => {
    if (companyData) {
      form.setFieldsValue({
        companyname: name,
        location,
        timezone,
      });
    }
  }, [companyData, form, location, name, timezone]);

  const handleFinish = (values) => {
    const { companyname, location, timezone } = values;
    setLoader(true);
    client
      .mutate({
        mutation: UPDATE_COMPANY,
        variables: { id, name: companyname, location, timezone },
      })
      .then((res) => {
        const updateDetails = res.data;
        const { name, location, timezone } = updateDetails.updateCompany;
        setCompanyData((prevState) => {
          return { ...prevState, name, location, timezone };
        });
        Modal.success({
          content: "Company Updated Successfully",
        });
        setLoader(false);
      })
      .catch((error) => {
        console.log(error);
        form.setFieldsValue({
          companyname: name,
          location,
          timezone,
        });
        Modal.error({
          title: "Invalid name, location and timezone",
          content: "Input valid name, location and timezone",
        });
        setLoader(false);
      });
  };

  const handleCancel = () => {
    form.setFieldsValue({
      companyname: name,
      location,
      timezone,
    });
  };

  return (
    <Form form={form} onFinish={handleFinish} layout="vertical">
      <Form.Item
        label="Company Name"
        name="companyname"
        rules={[
          {
            required: true,
            message: "Please input your company name!",
            whitespace: true,
          },
        ]}
      >
        <Input size="large" disabled={role === "Admin" ? false : true} />
      </Form.Item>

      <Form.Item
        label="Location"
        name="location"
        rules={[
          {
            required: true,
            message: "Please input location!",
            whitespace: true,
          },
        ]}
      >
        <Input size="large" disabled={role === "Admin" ? false : true} />
      </Form.Item>

      <Form.Item
        label="Time Zone"
        name="timezone"
        rules={[{ required: true, message: "Please select time zone!" }]}
      >
        <Select
          showSearch
          size="large"
          disabled={role === "Admin" ? false : true}
        >
          {timeZoneNames &&
            timeZoneNames.length &&
            timeZoneNames.map((_zone, index) => (
              <Option value={_zone} key={index}>
                {_zone}
              </Option>
            ))}
        </Select>
      </Form.Item>

      {role === "Admin" && (
        <Form.Item>
          <ButtonDiv>
            <Button
              className="cancelbtn"
              color="#FFFFFF"
              bgcolor="#999999"
              bordercolor="#999999"
              onClick={handleCancel}
            >
              Cancel
            </Button>
            <Button
              color="#FFFFFF"
              bgcolor="#3C3C84"
              bordercolor="#3C3C84"
              htmlType="submit"
              loading={loader}
              disabled={role === "Admin" ? false : true}
            >
              Save
            </Button>
          </ButtonDiv>
        </Form.Item>
      )}
    </Form>
  );
}

function CompanyProfile(props) {
  const { userData } = useContext(GetUserContext);
  const userdetails = userData || {};
  const { role = "" } = userdetails;
  return (
    <CompanyProfileWrapper>
      <Row>
        <Col
          xs={{ span: 8, offset: 8 }}
          sm={{ span: 8, offset: 8 }}
          md={{ span: 4, offset: 6 }}
          lg={{ span: 3, offset: 7 }}
          xl={{ span: 3, offset: 7 }}
        >
          <ProfilePic>
            <Avatar size={130} src={logoSrc} />
            {role === "Admin" && (
              <ChangeProfile>
                <Upload className="uploadclass">
                  <P color="#FFFFFF">Change</P>
                </Upload>
              </ChangeProfile>
            )}
          </ProfilePic>
        </Col>
        <Col xs={24} sm={24} md={{ span: 8, offset: 1 }} lg={7} xl={7}>
          <CompanyForm {...props} />
        </Col>
      </Row>
    </CompanyProfileWrapper>
  );
}

export default CompanyProfile;
