import React, { useState } from "react";
import {
  MainDiv,
  Wrapper,
  ImgText,
  LoginForm,
  ButtonLink,
  SignupLink,
} from "./css/LoginStyled";
import { P, Form, Input, Button, Modal, Select } from "../AntStyles";
import { Link } from "react-router-dom";
import { REGISTER } from "./graphql/Mutations";
import { GET_COMPANY } from "./graphql/Queries";
import client from "../../apollo";

const { Option } = Select;

function SignupForm() {
  const [form] = Form.useForm();
  const [loader, setLoader] = useState(false);
  const [companyId, setCompanyId] = useState("");
  const handleOnChange = value => {
    let name = value;
    client
      .query({
        query: GET_COMPANY,
        variables: { name },
      })
      .then(res => {
        if (res && res.data && res.data.getCompany) {
          let { id } = res.data.getCompany;
          setCompanyId(id);
        } else {
          console.log("No data find");
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  const handleFinish = values => {
    const { fullname, email } = values;
    setLoader(true);
    client
      .mutate({
        mutation: REGISTER,
        variables: { name: fullname, email, companyId },
      })
      .then(data => {
        Modal.success({
          content: "Check your Mail",
        });
        form.setFieldsValue({
          fullname: "",
          email: "",
          company: "",
        });
        setLoader(false);
      })
      .catch(error => {
        console.log(error);
        Modal.error({
          title: "Invalid Email",
          content: "Input valid email",
        });
        setLoader(false);
      });
  };

  return (
    <Form form={form} onFinish={handleFinish} layout="vertical">
      <Form.Item
        label="Full Name"
        name="fullname"
        rules={[{ required: true, message: "Please input your full name!" }]}
      >
        <Input size="large" />
      </Form.Item>

      <Form.Item
        label="Email"
        name="email"
        rules={[
          {
            type: "email",
            message: "The input is not valid E-mail!",
          },
          {
            required: true,
            message: "Please input your E-mail!",
          },
        ]}
      >
        <Input size="large" />
      </Form.Item>

      <Form.Item
        label="Company"
        name="company"
        rules={[{ required: true, message: "Please select your company!" }]}
      >
        <Select
          size="large"
          className="companySelector"
          onChange={handleOnChange}
        >
          <Option value="Logicwind">Logicwind</Option>
        </Select>
      </Form.Item>

      <Form.Item>
        <ButtonLink>
          <Button
            className="signupbtn"
            color="#FFFFFF"
            bgcolor="#3C3C84"
            bordercolor="#3C3C84"
            htmlType="submit"
            loading={loader}
          >
            Signup
          </Button>

          <SignupLink>
            <P color="#666666" fontsize="14px" fontweight="500">
              Already have an account?
            </P>
            <Link to="/login">
              <P color="#3C3C84" fontsize="14px" fontweight="500">
                <b>Login</b>
              </P>
            </Link>
          </SignupLink>
        </ButtonLink>
      </Form.Item>
    </Form>
  );
}

function Signup() {
  return (
    <MainDiv>
      <Wrapper>
        <ImgText>
          <div className="welcomemsg">
            <P color="#FFFFFF" fontsize="48px">
              Welcome Back!
            </P>
            <P color="#CCCCCC" fontsize="18px">
              To keep Connected with us
            </P>
            <P color="#CCCCCC" fontsize="18px">
              please login here with your details
            </P>
          </div>
        </ImgText>
        <LoginForm className="signup">
          <P color="#000000" fontsize="36px" fontweight="500">
            Signup To HRS
          </P>
          <SignupForm />
        </LoginForm>
      </Wrapper>
    </MainDiv>
  );
}

export default Signup;
