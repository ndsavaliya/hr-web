import React, { useState } from "react";
import { Input, Button, Form, Modal } from "../../AntStyles";
import { ButtonDiv } from "../css/SettingStyles";
import { CHANGE_PASSWORD } from "../graphql/Mutations";
import client from "../../../apollo";

function PasswordForm(props) {
  const currentUser = JSON.parse(localStorage.getItem("current_user"));
  const email = currentUser && currentUser.email;

  const [form] = Form.useForm();
  const [loader, setLoader] = useState(false);

  const handleFinish = (values) => {
    const { oldpassword, newpassword } = values;

    setLoader(true);
    client
      .mutate({
        mutation: CHANGE_PASSWORD,
        variables: {
          email,
          password: oldpassword,
          newPassword: newpassword,
        },
      })
      .then((data) => {
        Modal.success({
          title: "Password Updated Successfully",
        });
        form.setFieldsValue({
          oldpassword: "",
          newpassword: "",
          confirm: "",
        });
        setLoader(false);
      })
      .catch((error) => {
        console.log(error);
        Modal.error({
          content: "Password Can Not Updated",
        });
        setLoader(false);
      });
  };

  const handleCancel = () => {
    form.setFieldsValue({
      oldpassword: "",
      newpassword: "",
      confirm: "",
    });
  };

  return (
    <Form form={form} onFinish={handleFinish} layout="vertical">
      <Form.Item
        label="Old Password"
        name="oldpassword"
        rules={[
          {
            required: true,
            message: "Please input your old password!",
          },
        ]}
      >
        <Input type="password" size="large" />
      </Form.Item>

      <Form.Item
        label="New Password"
        name="newpassword"
        rules={[
          {
            required: true,
            message: "Please input your new password!",
          },
        ]}
      >
        <Input type="password" size="large" />
      </Form.Item>

      <Form.Item
        label="Confirm new Password"
        name="confirm"
        dependencies={["newpassword"]}
        rules={[
          {
            required: true,
            message: "Please confirm your password!",
          },
          ({ getFieldValue }) => ({
            validator(rule, value) {
              if (!value || getFieldValue("newpassword") === value) {
                return Promise.resolve();
              }
              return Promise.reject(
                "The two passwords that you entered do not match!"
              );
            },
          }),
        ]}
      >
        <Input type="password" size="large" />
      </Form.Item>

      <Form.Item xs={24} sm={12} md={12} lg={9} xl={9}>
        <ButtonDiv>
          <Button
            className="cancelbtn"
            color="#FFFFFF"
            bgcolor="#999999"
            bordercolor="#999999"
            onClick={handleCancel}
          >
            Cancel
          </Button>
          <Button
            color="#FFFFFF"
            bgcolor="#3C3C84"
            bordercolor="#3C3C84"
            htmlType="submit"
            loading={loader}
          >
            Save
          </Button>
        </ButtonDiv>
      </Form.Item>
    </Form>
  );
}

export default PasswordForm;
