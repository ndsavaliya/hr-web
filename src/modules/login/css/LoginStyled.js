import styled from "styled-components";

const flex = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
  font-family: "Poppins", sans-serif;
`;

export const MainDiv = styled.div`
  padding: 4%;
`;

export const Wrapper = styled.div`
  background: #ffffff;
  border-radius: 5px;
  height: 500px;
  box-shadow: 0px 0px 107px rgba(0, 0, 0, 0.06);
  margin: 0%;

  @media only screen and (min-width: 771px) {
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    justify-content: flex-start;
    align-items: stretch;
    align-content: stretch;
  }
`;

export const ImgText = styled(flex)`
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background: linear-gradient(rgba(60, 60, 132, 0.76), rgba(60, 60, 132, 0.76)),
    url("https://images.unsplash.com/photo-1461749280684-dccba630e2f6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80");
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  color: white;
  border-radius: 5px;

  & > .welcomemsg {
    text-align: center;
  }

  @media only screen and (max-width: 770px) {
    width: 100%;
    padding: 10%;
  }

  @media only screen and (min-width: 771px) {
    width: 55%;
  }
`;

export const LoginForm = styled.div`
  padding: 2% 5%;
  width: 45%;

  &.signup {
    padding: 1% 5%;
  }

  & > p {
    margin-bottom: 2%;
  }

  @media only screen and (max-width: 770px) {
    width: 100%;
  }

  @media only screen and (min-width: 771px) {
    width: 45%;
  }
`;

export const ButtonLink = styled(flex)`
  flex-direction: column;
  justify-content: center;
  align-items: center;

  & > .loginbtn {
    width: 250px;
    height: 55px;
    margin-bottom: 5%;
    font-size: 24px;
    font-weight: 500;
    text-transform: capitalize;
  }

  & > .signupbtn {
    width: 250px;
    height: 55px;
    margin-bottom: 2%;
    font-size: 24px;
    font-weight: 500;
    text-transform: capitalize;
  }
`;

export const SignupLink = styled(flex)``;
