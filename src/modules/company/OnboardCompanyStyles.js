import styled from "styled-components";

export const flex = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
  font-family: "Poppins", sans-serif;
`;

export const CompanyWrapper = styled(flex)`
  flex-direction: column;
  padding: 10px;
`;

export const ProfileHeader = styled(flex)`
  flex-direction: column;
  background: #f0f0fa;
  border-radius: 4px;
`;

export const LogoNameLocation = styled(flex)`
  align-items: center;
  align-content: center;
  margin: 3% 5%;
`;

export const NameLocationTimeZone = styled(flex)`
  flex-direction: column;
  margin: 0% 1.5%;
`;

export const ProfilePic = styled.div`
  &.profilepic {
    @media screen and (max-width: 450px) {
      height: 100px;
      width: 130px;
    }

    @media screen and (min-width: 451px) {
      height: 130px;
      width: 130px;
    }
  }

  &.profilepic > span {
    height: 100%;
    width: 100%;
  }

  cursor: pointer;
  overflow: hidden;
  border-radius: 50%;
`;

export const ChangeProfile = styled.div`
  background: linear-gradient(transparent 50%, rgba(0, 0, 0, 0.6) 50%);
  width: 100%;
  height: 100%;
  position: relative;
  display: flex;
  top: -100%;

  p {
    margin-top: 55%;
    margin-left: 10%;
    margin-right: 10%;
    font-size: 12px;
    text-align: center;
  }
`;
