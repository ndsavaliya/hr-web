import gql from "graphql-tag";

export const GET_CURRENT_USER = gql`
  query getCurretUser {
    currentUser {
      id
      name
      profileImage
      role
      email
      birthdate
      password
      companyId
      isDeleted
      resetPasswordToken
      resetPasswordExpires
      token
      punchStatus
    }
  }
`;

export const GET_USER = gql`
  query getUser($id: ID!) {
    getUser(where: { id: $id }) {
      id
      name
      profileImage
      role
      email
      birthdate
      password
      companyId
      isDeleted
      resetPasswordToken
      resetPasswordExpires
      token
      punchStatus
    }
  }
`;

export const ALL_USERS = gql`
  query getAllUsers {
    allUsers {
      id
      employeeId
      name
      profileImage
      role
      email
      birthdate
      password
      companyId
      isDeleted
      emailVerified
      resetPasswordToken
      resetPasswordExpires
      registerToken
      registerTokenExpires
      token
      joiningDate
      deletedAt
      punchStatus
    }
  }
`;

export const GET_USER_AVAILABILITY = gql`
  query getUserAvailability {
    userAvailability {
      id
      user {
        id
        name
        profileImage
      }
      leaves {
        type
        date
      }
      status
    }
  }
`;

export const GET_USER_PUNCHES = gql`
  query getUserPunches($userId: ID!) {
    userPunches(userId: $userId) {
      checkIn
      checkOut
    }
  }
`;

export const ALL_HOLIDAYS = gql`
  query getAllHolidays($companyId: ID!) {
    allHolidays(companyId: $companyId) {
      id
      name
      date
      companyId
      deletedAt
    }
  }
`;

export const HOLIDAY = gql`
  query getOneHoliday($id: ID!) {
    getHoliday(id: $id) {
      id
      name
      date
      companyId
      deletedAt
    }
  }
`;

export const GET_ALL_LEAVE_REQUEST = gql`
  query getAllLeaveRequest($userId: ID, $assignmentId: ID) {
    getAllLeaveRequest(
      where: { userId: $userId, assignmentId: $assignmentId }
    ) {
      id
      requestType
      user {
        id
        name
        profileImage
        email
      }
      leaves {
        id
        type
        date
        leaveRequestId
      }
      status
      createdAt
    }
  }
`;

export const GET_LEAVE_REQUEST = gql`
  query getLeaveRequest($id: ID!) {
    getLeaveRequest(id: $id) {
      id
      user {
        id
        name
        profileImage
        email
      }
      createdAt
      requestType
      leaves {
        id
        type
        date
      }
      description
    }
  }
`;

export const GET_NOTIFICATION = gql`
  query getNotification {
    getNotification {
      id
      refData
      isViewed
    }
  }
`;

export const LISTING_LEAVES = gql`
  query listingLeaveRequests {
    listingLeaveRequests {
      id
      user {
        name
        profileImage
        role
        email
      }
      status
      isCancelled
      leaves {
        date
        type
      }
    }
  }
`;

export const LEAVE_SETTING = gql`
  query getLeaveSetting($companyId: ID!) {
    getLeaveSetting(companyId: $companyId) {
      id
      companyId
      leaveTypes {
        id
        type
      }
    }
  }
`;
