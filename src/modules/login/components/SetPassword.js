import React, { useState, useEffect } from "react";
import {
  MainDiv,
  Wrapper,
  ImgText,
  LoginForm,
  ButtonLink,
} from "../css/LoginStyled";
import { P, Form, Input, Button, Modal } from "../../AntStyles";
import { SET_PASSWORD } from "../graphql/Mutations";
import client from "../../../apollo";

function SetPasswordForm(props) {
  console.log(props);
  const [form] = Form.useForm();
  const [loader, setLoader] = useState(false);
  const [tokenId, setTokenId] = useState("");

  useEffect(() => {
    let token_id = props.token;
    setTokenId(token_id);
  }, [props.token]);

  const handleFinish = values => {
    const { password } = values;
    setLoader(true);
    client
      .mutate({
        mutation: SET_PASSWORD,
        variables: { registerToken: tokenId, newPassword: password },
      })
      .then(data => {
        Modal.success({
          title: "Password Created Successfully",
          content: "Go Back to Login Page",
        });
        form.setFieldsValue({
          password: "",
          confirm: "",
        });
        setLoader(false);
      })
      .catch(error => {
        console.log(error);
        Modal.error({
          title: "Password Not Created",
        });
        setLoader(false);
      });
  };

  return (
    <Form form={form} onFinish={handleFinish} layout="vertical">
      <Form.Item
        label="Password"
        name="password"
        rules={[
          {
            required: true,
            message: "Please input your password!",
          },
        ]}
      >
        <Input type="password" size="large" />
      </Form.Item>

      <Form.Item
        label="Confirm Password"
        name="confirm"
        dependencies={["password"]}
        rules={[
          {
            required: true,
            message: "Please confirm your password!",
          },
          ({ getFieldValue }) => ({
            validator(rule, value) {
              if (!value || getFieldValue("password") === value) {
                return Promise.resolve();
              }
              return Promise.reject(
                "The two passwords that you entered do not match!",
              );
            },
          }),
        ]}
      >
        <Input type="password" size="large" />
      </Form.Item>

      <Form.Item>
        <ButtonLink>
          <Button
            className="loginbtn"
            color="#FFFFFF"
            bgcolor="#3C3C84"
            bordercolor="#3C3C84"
            htmlType="submit"
            loading={loader}
          >
            Submit
          </Button>
        </ButtonLink>
      </Form.Item>
    </Form>
  );
}

function SetPassword(props) {
  return (
    <MainDiv>
      <Wrapper>
        <ImgText>
          <div className="welcomemsg">
            <P color="#FFFFFF" fontsize="48px">
              Welcome Back!
            </P>
            <P color="#CCCCCC" fontsize="18px">
              To keep Connected with us
            </P>
            <P color="#CCCCCC" fontsize="18px">
              please login here with your details
            </P>
          </div>
        </ImgText>
        <LoginForm>
          <P color="#000000" fontsize="36px" fontweight="500">
            Create Password To HRS
          </P>
          <SetPasswordForm token={props.match.params.token} />
        </LoginForm>
      </Wrapper>
    </MainDiv>
  );
}

export default SetPassword;
