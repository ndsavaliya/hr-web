import React from "react";
import Employee from "./Employee";
import { Row, Spin, Result } from "antd";
import { EmployeesWrapper } from "../css/EmployeesLoggedInStyles";
import { useQuery } from "react-apollo";
import { ALL_USERS } from "../graphql/Queries";

function EmployeesLoggedIn() {
  const { loading, error, data } = useQuery(ALL_USERS, {
    fetchPolicy: "network-only",
  });

  if (loading)
    return (
      <Spin
        size="large"
        style={{
          width: "3rem",
          height: "3rem",
          position: "absolute",
          left: "50%",
          top: "50%",
        }}
      />
    );
  if (error) return <Result status="error" title="Error" />;

  const allUserData = data.allUsers;

  return (
    <div style={{ width: "100%" }}>
      <EmployeesWrapper>
        <Row>
          {allUserData &&
            allUserData.length &&
            allUserData.map(userData => (
              <Employee key={userData.id} userData={userData} />
            ))}
        </Row>
      </EmployeesWrapper>
    </div>
  );
}

export default EmployeesLoggedIn;
