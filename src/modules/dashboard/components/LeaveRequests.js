import React from "react";
import { LeaveReqWrapper, LeaveReqHeader } from "../css/LeaveRequestsStyles";
import { P, TabBar } from "../../AntStyles";
import Approvals from "./Approvals";
import MyLeaves from "./MyLeaves";
import RequestLeaveModel from "./RequestLeaveModel";

const { TabPane } = TabBar;

function LeaveRequests() {
  return (
    <LeaveReqWrapper>
      <LeaveReqHeader>
        <P color="#333333" fontsize="24px" fontweight="500">
          Leave Requests
        </P>
        <RequestLeaveModel />
      </LeaveReqHeader>
      <div>
        <TabBar
          destroyInactiveTabPane
          defaultActiveKey="1"
          className="leavereqtabflex"
        >
          <TabPane tab="Approvals" key="1" className="tab_class">
            <Approvals />
          </TabPane>
          <TabPane tab="My Leaves" key="2" className="tab_class">
            <MyLeaves />
          </TabPane>
        </TabBar>
      </div>
    </LeaveReqWrapper>
  );
}

export default LeaveRequests;
