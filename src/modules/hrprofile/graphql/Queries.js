import gql from "graphql-tag";

export const GET_CURRENT_USER = gql`
  query getCurretUser {
    currentUser {
      id
      name
      profileImage
      role
      email
      birthdate
      password
      companyId
      isDeleted
      resetPasswordToken
      resetPasswordExpires
      token
      punchStatus
    }
  }
`;

export const GET_USER = gql`
  query getUser($id: ID!) {
    getUser(where: { id: $id }) {
      id
      employeeId
      name
      profileImage
      role
      email
      birthdate
      joiningDate
      password
      companyId
      isDeleted
      resetPasswordToken
      resetPasswordExpires
      token
      punchStatus
    }
  }
`;

export const ALL_USERS = gql`
  query getAllUsers {
    allUsers {
      id
      employeeId
      name
      profileImage
      role
      email
      birthdate
      joiningDate
      password
      companyId
      isDeleted
      emailVerified
      resetPasswordToken
      resetPasswordExpires
      registerToken
      registerTokenExpires
      token
      deletedAt
      punchStatus
    }
  }
`;

export const ALL_HOLIDAYS = gql`
  query getAllHolidays($companyId: ID!) {
    allHolidays(companyId: $companyId) {
      id
      name
      date
      companyId
      deletedAt
    }
  }
`;

export const HOLIDAY = gql`
  query getOneHoliday($id: ID!) {
    getHoliday(id: $id) {
      id
      name
      date
      companyId
      deletedAt
    }
  }
`;
