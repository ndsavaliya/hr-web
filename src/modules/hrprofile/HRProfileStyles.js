import styled from "styled-components";

const flex = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
  font-family: "Poppins", sans-serif;
`;

export const HRWrapper = styled(flex)`
  flex-direction: column;
  padding: 10px;
`;

export const ProfileHeader = styled(flex)`
  flex-direction: column;
  background: #f0f0fa;
  border-radius: 4px;
`;

export const ProfileImgNamePos = styled(flex)`
  justify-content: center;
  align-content: center;
  margin: 3% 0%;
`;

export const HRNamePos = styled(flex)`
  flex-direction: column;
  margin: 0% 1.5%;
`;
