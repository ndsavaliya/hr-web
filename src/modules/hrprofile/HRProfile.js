import React, { useContext } from "react";
import ProfileAndVisibility from "./components/ProfileAndVisibility";
import Setting from "./components/Setting";
import Company from "./components/Company";
// import Calendar from "./components/Calendar";
import {
  HRWrapper,
  ProfileHeader,
  ProfileImgNamePos,
  HRNamePos,
} from "./HRProfileStyles";
import { P, TabBar, Avatar } from "../AntStyles";
import { startCase } from "lodash";
import { GetUserContext } from "../UserContextProvider";
import { UserOutlined } from "@ant-design/icons";

const { TabPane } = TabBar;

function HRProfile() {
  const { userData } = useContext(GetUserContext);
  const details = userData || {};
  const { name = "", role = "", profileImage = "" } = details;

  return (
    <HRWrapper>
      <ProfileHeader>
        <ProfileImgNamePos>
          <div>
            {profileImage ? (
              <Avatar size={90} src={profileImage} />
            ) : (
              <Avatar size={90} icon={<UserOutlined />} />
            )}
          </div>
          <HRNamePos>
            <P color="#3C3C84" fontsize="36px" fontweight="500">
              {startCase(name)}
            </P>
            <P color="#666666" fontsize="18px" fontweight="300">
              {role}
            </P>
          </HRNamePos>
        </ProfileImgNamePos>

        <TabBar defaultActiveKey="1">
          <TabPane tab="Profile And Visibility" key="1">
            <ProfileAndVisibility />
          </TabPane>
          <TabPane tab="Settings" key="2">
            <Setting />
          </TabPane>
          <TabPane tab="Company" key="3">
            <Company />
          </TabPane>
          {/* <TabPane tab="Calendar" key="4">
            <Calendar />
          </TabPane> */}
        </TabBar>
      </ProfileHeader>
    </HRWrapper>
  );
}

export default HRProfile;
