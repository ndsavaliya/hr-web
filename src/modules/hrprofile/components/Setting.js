import React from "react";
import { Collapse, Radio, Divider, Switch, P, Form } from "../../AntStyles";
import { Row, Col } from "antd";
import { SettingWrapper, AllowDesNoti } from "../css/SettingStyles";
import PasswordForm from "./PasswordForm";
import { RightOutlined } from "@ant-design/icons";

const { Panel } = Collapse;

function RadioForm() {
  return (
    <Form>
      <Form.Item name="radio-group">
        <Radio.Group>
          <Row>
            <Col xs={24} lg={6}>
              <Radio value="never">
                <P color="#3C3C84" fontsize="18px">
                  Never
                </P>
                <P color="#3C3C84">Don't send notification.</P>
              </Radio>
            </Col>
            <Col xs={24} lg={9}>
              <Radio value="periodically">
                <P color="#3C3C84" fontsize="18px">
                  Periodically
                </P>
                <P color="#3C3C84">Send notification about once in hour.</P>
              </Radio>
            </Col>
            <Col xs={24} lg={9}>
              <Radio value="instantly">
                <P color="#3C3C84" fontsize="18px">
                  Instantly
                </P>
                <P color="#3C3C84">Send notification as soon as possible.</P>
              </Radio>
            </Col>
          </Row>
        </Radio.Group>
      </Form.Item>
    </Form>
  );
}

function Setting() {
  return (
    <SettingWrapper>
      <Row>
        <Col
          md={{ span: 22, offset: 1 }}
          lg={{ span: 20, offset: 2 }}
          xl={{ span: 20, offset: 2 }}
        >
          <P color="#3C3C84" fontsize="24px" fontweight="500">
            Account Details
          </P>
          <Divider />

          <Collapse
            className="abc"
            bordered={false}
            defaultActiveKey={["1"]}
            expandIcon={({ isActive }) => (
              <RightOutlined rotate={isActive ? 90 : 0} />
            )}
            expandIconPosition="right"
          >
            <Panel header="Change Password" className="collapse_header" key="1">
              <Row>
                <Col xs={24} sm={12} md={12} lg={9} xl={9}>
                  <PasswordForm />
                </Col>
              </Row>
            </Panel>
          </Collapse>
        </Col>
        <Col
          md={{ span: 22, offset: 1 }}
          lg={{ span: 20, offset: 2 }}
          xl={{ span: 20, offset: 2 }}
        >
          <P color="#3C3C84" fontsize="24px" fontweight="500">
            Notification Settings
          </P>
          <Divider />
          <Collapse
            className="abc"
            bordered={false}
            defaultActiveKey={["2"]}
            expandIcon={({ isActive }) => (
              <RightOutlined rotate={isActive ? 90 : 0} />
            )}
            expandIconPosition="right"
          >
            <Panel
              header="Change Notification Email Frequency..."
              className="collapse_header"
              key="2"
            >
              <RadioForm />
            </Panel>
          </Collapse>

          <AllowDesNoti>
            <div>
              <P color="#666666" fontsize="18px" fontweight="500">
                Allow Desktop Notifications
              </P>
            </div>
            <div>
              <Switch defaultChecked />
            </div>
          </AllowDesNoti>
        </Col>
      </Row>
    </SettingWrapper>
  );
}

export default Setting;
