import styled from "styled-components";
import {
  Button as ButtonAnt,
  Avatar as AvatarAnt,
  Tag as TagAnt,
  Statistic as StatisticAnt,
  Badge as BadgeAnt,
  Popover as PopoverAnt,
  Tabs as TabsAnt,
  Dropdown as DropdownAnt,
  Collapse as CollapseAnt,
  Modal as ModalAnt,
  Table as TableAnt,
  Input as InputAnt,
  Radio as RadioAnt,
  Divider as DividerAnt,
  Switch as SwitchAnt,
  Select as SelectAnt,
  DatePicker as DatePickerAnt,
  TimePicker as TimePickerAnt,
  Form as FormAnt,
} from "antd";

export const H1 = styled.h1`
  color: ${(props) => props.color};
  margin: 0%;
`;

export const H2 = styled.h2`
  color: ${(props) => props.color};
  margin: 0%;
`;

export const H3 = styled.h3`
  color: ${(props) => props.color};
  margin: 0%;
  font-size: ${(props) => props.fontsize};
`;

export const H4 = styled.h4`
  color: ${(props) => props.color};
  margin: 0%;
`;

export const H5 = styled.h5`
  color: ${(props) => props.color};
  margin: 0%;
`;

export const P = styled.p`
  color: ${(props) => props.color};
  font-size: ${(props) => props.fontsize};
  font-weight: ${(props) => props.fontweight};
  margin: 0px;
`;

export const Button = styled(ButtonAnt)`
  text-transform: uppercase;
  font-size: 12px;
  font-weight: 500;
  margin-left: ${(props) => props.marginleft};

  &,
  &:hover,
  &:focus {
    color: ${(props) => props.color};
    border-color: ${(props) => props.bordercolor};
    background-color: ${(props) => props.bgcolor};
  }
`;

export const Avatar = styled(AvatarAnt)`
  border: ${(props) => props.bordersizestylecolor};
  margin-right: ${(props) => props.marginright};
  background: ${(props) => props.bgcolor};
`;

export const Days = styled.div`
  border-radius: 50px;
  height: ${(props) => props.height_width};
  width: ${(props) => props.height_width};
  position: relative;
  margin: 1%;
  border: 1px solid transparent;

  &.current_day {
    border-color: #666666;
  }

  &.no_leave {
    background: #bbd88c;
  }

  &.firsthalf_leave {
    background: linear-gradient(90deg, #efb9b9 50%, #bbd88c 50%);
  }

  &.sechalf_leave {
    background: linear-gradient(90deg, #bbd88c 50%, #efb9b9 50%);
  }

  &.full_leave {
    background: #efb9b9;
  }

  &.tomorrow {
    background: transparent;
    border-color: #cccccc;
  }

  &.holiday {
    background: #cccccc;
  }

  &.year_holiday {
    background: #3c3c84;
    margin-right: 10px;
  }

  & > p {
    text-align: center;
    color: ${(props) => props.color};
    font-size: ${(props) => props.fontsize};
    font-weight: ${(props) => props.fontweight};
    margin-top: 10%;
  }

  & > h3 {
    text-align: center;
    color: ${(props) => props.color};
    font-size: 18px;
    margin-top: 5px;
  }
`;

export const Tag = styled(TagAnt)`
  font-weight: 500;
  font-size: 10px;
  color: #ffffff;
`;

export const Statistic = styled(StatisticAnt)`
  text-align: center;

  .ant-statistic-title {
    font-weight: 500;
    font-size: 14px;
    color: #000000;
  }

  .ant-statistic-content-value {
    font-weight: 500;
    font-size: 36px;
    color: #000000;
  }
`;

export const Badge = styled(BadgeAnt)`
  &.notificationBadge {
    margin-left: 2%;
  }
`;

export const Popover = styled(PopoverAnt)`
  background: red;
`;

export const TabBar = styled(TabsAnt)`
  .ant-tabs-nav-container {
    border-bottom: solid 0.5px #cccccc;
  }

  & > div > div > .ant-tabs-nav-wrap {
    display: flex;
    justify-content: center;
  }

  .ant-tabs-nav-scroll {
    overflow: initial;
  }

  &.companytabflex > div > div > .ant-tabs-nav-wrap {
    display: block;
    padding-left: 2%;
  }

  &.leavereqtabflex > div > div > .ant-tabs-nav-wrap {
    display: block;
  }

  .ant-tabs-nav .ant-tabs-tab {
    color: #3c3c84;
    font-size: 18px;
  }

  .ant-tabs-tab:hover {
    color: #3c3c84;
  }

  .ant-tabs-bar {
    margin: 20px 0px 0px 0px;
  }

  .ant-tabs-nav .ant-tabs-tab-active {
    color: #ffffff;
    background-color: #3c3c84;
    border-radius: 5px;
    padding-left: 2%;
    padding-right: 2%;
  }

  .ant-tabs-ink-bar {
    background-color: #3c3c84;
  }
`;

export const Dropdown = styled(DropdownAnt)`
  &.notification > .ant-dropdown {
    height: 100px;
  }
`;

export const Collapse = styled(CollapseAnt)`
  .ant-collapse-content-box {
    background: #ffffff;
  }

  .ant-collapse > .ant-collapse-item > .ant-collapse-header {
    color: red;
  }

  & > .collapse_header {
    font-size: 18px;
    font-weight: 500;
    background: #f0f0fa;
    border-radius: 40px;
    margin-bottom: 24px;
    border: 0px;
    & > .ant-collapse-header {
      color: #666666;
    }
  }

  .ant-row {
    margin-top: 2%;
  }
`;

export const Modal = styled(ModalAnt)`
  .ant-modal-header {
    background: #3c3c84;

    .ant-modal-title {
      font-size: 24px;
      font-weight: 500;
      color: #ffffff;
    }
  }

  .ant-modal-title {
    color: #ffffff;
  }

  .ant-modal-close-x {
    margin-top: -110%;
    line-height: 26px;
    height: 25px;
    width: 25px;
    border-radius: 50%;
    background: #ffffff;
  }
`;

export const Table = styled(TableAnt)`
  .ant-table thead > tr > th {
    background: none;
  }

  td {
    font-size: 14px;
    color: #666666;
  }

  & > div > div > div > div > div > table > tbody > tr > .table_left_border {
    border-left: 1px solid rgba(204, 204, 204, 0.5);
  }

  & > div > div > div > div > div > table > tbody > tr > .table_right_border {
    border-right: 1px solid rgba(204, 204, 204, 0.5);
  }
`;

export const Input = styled(InputAnt).attrs((props) => ({
  type: props.type,
}))`
  border-radius: 5px;
  width: 100%;

  &.input80 {
    width: 80%;
  }

  &.input50 {
    width: 50%;
  }
`;

export const Radio = styled(RadioAnt).attrs((props) => ({
  value: props.value,
}))`
  &.ant-radio-wrapper {
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
  }

  .ant-radio-inner {
    border: 1px solid #3c3c84 !important;
  }

  .ant-radio-inner::after {
    background-color: #3c3c84 !important;
  }

  .ant-radio-checked::after {
    border: 0px none transparent !important;
  }
`;

export const Divider = styled(DividerAnt)`
  margin: 10px 0px;
  border: 0.5px solid #cccccc;
`;

export const Switch = styled(SwitchAnt).attrs((props) => ({
  defaultChecked: props.defaultChecked,
}))`
  background: #3c3c84;
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.24), 0px 0px 2px rgba(0, 0, 0, 0.12);
`;

export const Select = styled(SelectAnt)`
  width: 100%;
  &.selectBorderRadius > .ant-select-selector {
    border-radius: 5px;
  }

  &.cancel_type > .ant-select-selector {
    border-radius: 0px;
    border-top: none;
    border-left: none;
    border-right: none;
  }

  &.companySelector > .ant-select-selector {
    border-radius: 5px;
  }

  &.leave_type > .ant-select-selector > .ant-select-selection-item {
    border-radius: 20px;
    background: #ffffff;
  }

  &.leave_type > .ant-select-selector {
    border-radius: 0px;
    border-top: none;
    border-left: none;
    border-right: none;
  }

  &.leave_time > .ant-select-selector {
    border-radius: 0px;
    border-top: none;
    border-left: none;
    border-right: none;
    width: 200px;
  }
`;

export const DatePicker = styled(DatePickerAnt).attrs(() => ({
  placeholder: "",
}))`
  &.dateBorderRadius {
    border-radius: 5px;
  }

  &.datepicker_class {
    border-radius: 0px;
    border-top: none;
    border-left: none;
    border-right: none;
    width: 100%;
  }
`;

export const TimePicker = styled(TimePickerAnt).attrs(() => ({
  placeholder: "",
}))`
  height: 40px;
`;

export const Form = styled(FormAnt)`
  font-family: "Poppins", sans-serif;

  .ant-form-item-required::before {
    display: inline-block;
    margin-right: 4px;
    position: absolute;
    color: #f5222d;
    margin-left: 100%;
    font-size: 14px;
    font-family: SimSun, sans-serif;
    line-height: 1;
    content: "*";
  }

  .ant-form-item-label > label {
    color: #666666;
    font-size: 18px;
  }
`;
