import React, { useState, useEffect, useContext } from "react";
import {
  ImgNameTime,
  LeaveDuration,
  LeaveReason,
} from "../css/ApprovalsStyles";
import { Button, P, Avatar, Tag, Modal } from "../../AntStyles";
import client from "../../../apollo";
import {
  GET_LEAVE_REQUEST,
  GET_ALL_LEAVE_REQUEST,
  GET_NOTIFICATION,
} from "../graphql/Queries";
import moment from "moment";
import { createMarkup } from "../../../utils";
import {
  RESPONSE_LEAVE_REQUEST,
  UPDATE_NOTIFICATION,
} from "../graphql/Mutations";
import { GetUserContext } from "../../UserContextProvider";
import { UserOutlined } from "@ant-design/icons";

function RespondLeaveModel(props) {
  const { leaveId } = props;
  const { userData } = useContext(GetUserContext);
  const details = userData || {};
  const { id = "" } = details;

  const [approveLoader, setApproveLoader] = useState(false);
  const [rejectLoader, setRejectLoader] = useState(false);
  const [resLeaveLoader, setResLeaveLoader] = useState(false);
  const [resVisible, setResVisible] = useState(false);
  const [leaveData, setLeaveData] = useState({});
  const [notificationData, setNotificationData] = useState({});

  useEffect(() => {
    if (leaveId) {
      client
        .query({
          query: GET_LEAVE_REQUEST,
          variables: { id: leaveId },
        })
        .then((res) => {
          let details = [];
          if (res && res.data && res.data.getLeaveRequest) {
            details = res.data.getLeaveRequest;
          }
          setLeaveData(details);
        })
        .catch((e) => {
          console.log(e);
        });
    }
  }, [leaveId]);

  useEffect(() => {
    if (leaveId) {
      client
        .query({
          query: GET_NOTIFICATION,
        })
        .then((res) => {
          setNotificationData(res.data.getNotification);
        })
        .catch((e) => {
          console.log(e);
        });
    }
  }, [leaveId]);

  const showModalRes = () => {
    const notifdata = notificationData.filter(
      (notification) => notification.refData.id === leaveId,
    );
    if (notifdata && notifdata.length && notifdata[0].id) {
      client
        .mutate({
          mutation: UPDATE_NOTIFICATION,
          variables: {
            id: notifdata[0].id,
          },
          refetchQueries: [
            {
              query: GET_NOTIFICATION,
              fetchPolicy: "network-only",
            },
          ],
        })
        .then((res) => {})
        .catch((error) => {
          console.log(error);
        });
    }
    setResLeaveLoader(true);
    setResVisible(true);
  };

  const handleResApprove = (e) => {
    setApproveLoader(true);
    client
      .mutate({
        mutation: RESPONSE_LEAVE_REQUEST,
        variables: { id: leaveId, status: "APPROVE" },
        refetchQueries: [
          {
            query: GET_ALL_LEAVE_REQUEST,
            variables: { assignmentId: id },
            fetchPolicy: "network-only",
          },
        ],
      })
      .then((res) => {
        setApproveLoader(false);
      })
      .catch((error) => {
        console.log(error);
        setApproveLoader(false);
      });
    setResLeaveLoader(false);
    setResVisible(false);
  };

  const handleResReject = (e) => {
    client
      .mutate({
        mutation: RESPONSE_LEAVE_REQUEST,
        variables: { id: leaveId, status: "REJECT" },
        refetchQueries: [
          {
            query: GET_ALL_LEAVE_REQUEST,
            variables: { assignmentId: id },
            fetchPolicy: "network-only",
          },
        ],
      })
      .then((res) => {
        setRejectLoader(false);
      })
      .catch((error) => {
        console.log(error);
        setRejectLoader(false);
      });
    setResLeaveLoader(false);
    setResVisible(false);
  };

  const handleReqCancel = (e) => {
    setResLeaveLoader(false);
    setResVisible(false);
  };

  return (
    <div>
      <Button
        color="#FFFFFF"
        bordercolor="#3C3C84"
        bgcolor="#3C3C84"
        onClick={showModalRes}
        loading={resLeaveLoader}
      >
        RESPOND
      </Button>
      <Modal
        className="modalFont"
        title="Leave Response"
        visible={resVisible}
        onApprove={handleResApprove}
        onCancel={handleReqCancel}
        footer={[
          <Button
            key="reject"
            color="#FFFFFF"
            bordercolor="#999999"
            bgcolor="#999999"
            loading={rejectLoader}
            onClick={handleResReject}
          >
            REJECT
          </Button>,
          <Button
            key="approve"
            color="#FFFFFF"
            bordercolor="#3C3C84"
            bgcolor="#3C3C84"
            onClick={handleResApprove}
            loading={approveLoader}
          >
            APPROVE
          </Button>,
        ]}
      >
        <div>
          <ImgNameTime>
            {leaveData && leaveData.user ? (
              leaveData.user && leaveData.user.profileImage ? (
                <Avatar
                  marginright="10px"
                  size={70}
                  src={leaveData.user.profileImage}
                />
              ) : (
                <Avatar marginright="10px" size={70} icon={<UserOutlined />} />
              )
            ) : (
              "User not available"
            )}
            <div>
              <P color="#333333" fontsize="18px" fontweight="500">
                {leaveData && leaveData.user
                  ? leaveData.user && leaveData.user.name
                    ? leaveData.user.name
                    : "Name not available"
                  : "User not available"}
              </P>
              <P color="#666666" fontsize="12px" fontweight="300">
                Requested on{" "}
                {leaveData &&
                  leaveData.createdAt &&
                  moment(leaveData.createdAt).format("DD/MM/YYYY h:mm a")}
              </P>
            </div>
          </ImgNameTime>
          <LeaveDuration>
            <Tag color="#3C3C84">{leaveData && leaveData.requestType}</Tag>
            <P color="#3C3C84" fontsize="12px" fontweight="500">
              Duration:{" "}
            </P>
            {leaveData && leaveData.leaves && leaveData.leaves.length
              ? leaveData.leaves.map((leave, index) => {
                  let date = "";
                  if (leave && leave.date) {
                    date = moment(leave.date).format("DD/MM/YYYY");
                  }
                  return (
                    <P
                      color="#3C3C84"
                      fontsize="12px"
                      fontweight="500"
                      key={index}
                    >
                      {date}
                    </P>
                  );
                })
              : "No Dates Found"}
          </LeaveDuration>
          <LeaveReason>
            <P fontsize="14px" fontweight="500">
              Reason:
            </P>
            <P
              fontsize="12px"
              fontweight="300"
              id="leave_reason"
              dangerouslySetInnerHTML={createMarkup(
                leaveData && leaveData.description
                  ? leaveData.description
                  : "<p></p>",
              )}
            />
          </LeaveReason>
        </div>
      </Modal>
    </div>
  );
}

export default RespondLeaveModel;
