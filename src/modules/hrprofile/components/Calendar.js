import React from "react";
import {
  CalendarWrapper,
  CalendarDiv,
  Legends,
  LeaveColor,
  LeaveType,
} from "../css/CalendarStyles";
import { ResponsiveCalendar } from "@nivo/calendar";
import moment from "moment";

// const present = 0;
// const Full_Day = 1;
// const First_Half = 2;
// const Sec_Half = 3;

const data = [
  {
    day: "2020-01-23",
    value: 1,
    // value: { Full_Day },
  },
  {
    day: "2020-02-09",
    value: 2,
    // value: {  First_Half },
  },
  {
    day: "2020-01-17",
    value: 3,
    // value: { Sec_Half },
  },
  {
    day: "2020-10-17",
    value: 1,
    // value: { Full_Day },
  },
  {
    day: "2020-03-02",
    value: 2,
    // value: { First_Half },
  },
  {
    day: "2020-07-28",
    value: 0,
    // value: { Sec_Half },
  },
];

const yearStart = moment().startOf("year").format("YYYY-MM-DD");
const yearEnd = moment().endOf("year").format("YYYY-MM-DD");

const fh = "linear-gradient(90deg, #efb9b9 50%, #bbd88c 50%)";
// console.log(fh);
const MyResponsiveCalendar = () => (
  <ResponsiveCalendar
    data={data}
    from={yearStart}
    to={yearEnd}
    emptyColor="#eeeeee"
    colors={["#bbd88c", "#efb9b9", `${fh}`, "#e8c1a0"]}
    margin={{ top: 40, right: 40, bottom: 40, left: 40 }}
    yearSpacing={40}
    monthBorderColor="#ffffff"
    dayBorderWidth={2}
    dayBorderColor="#ffffff"
    // legends={[
    //   {
    //     anchor: "bottom-right",
    //     direction: "row",
    //     translateY: 36,
    //     itemCount: 4,
    //     itemWidth: 42,
    //     itemHeight: 36,
    //     itemsSpacing: 14,
    //     itemDirection: "right-to-left",
    //   },
    // ]}
  />
);

function Calendar() {
  return (
    <CalendarWrapper>
      <CalendarDiv>
        <MyResponsiveCalendar />
      </CalendarDiv>
      <Legends>
        {/* <svg>
          <linearGradient id="lg">
            <stop style={{ stopColor: "#bbd88c" }} id="color1" offset="0" />
            <stop style={{ stopColor: "#efb9b9" }} id="color2" offset="1" />
          </linearGradient>
          <circle cx="50" cy="50" r="45" fill="url(#lg)" />
        </svg> */}
        <LeaveType>
          Present
          <LeaveColor className="present" />
        </LeaveType>
        <LeaveType>
          Full Day
          <LeaveColor className="fullDay" />
        </LeaveType>
        <LeaveType>
          First Half
          <LeaveColor className="firstHalf" />
        </LeaveType>
        <LeaveType>
          Sec Half
          <LeaveColor className="SecHalf" />
        </LeaveType>
      </Legends>
    </CalendarWrapper>
  );
}

export default Calendar;
