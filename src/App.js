import React from "react";
import { Switch, Route } from "react-router-dom";
import Login from "./modules/login/Login";
import Signup from "./modules/login/Signup";
import ForgotPassword from "./modules/login/components/ForgotPassword";
import ResetPassword from "./modules/login/components/ResetPassword";
import SetPassword from "./modules/login/components/SetPassword";
import GuardedRoute from "./modules/login/components/GuardedRoute";
import Home from "./modules/dashboard/Home";

function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/signup" component={Signup} />
        <Route exact path="/rg/:token" component={SetPassword} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/forgot-password" component={ForgotPassword} />
        <Route exact path="/rs/:token" component={ResetPassword} />

        <GuardedRoute path="/" component={Home} />
      </Switch>
    </div>
  );
}

export default App;
