import React, { useState, useEffect, useContext } from "react";
import { Button, Modal, Form, P } from "../../AntStyles";
import { LeaveType, DateTime } from "../css/LeaveRequestsStyles";
import moment from "moment";
import { DeleteOutlined } from "@ant-design/icons";
import { CANCEL_LEAVE } from "../graphql/Mutations";
import { GET_ALL_LEAVE_REQUEST } from "../graphql/Queries";
import client from "../../../apollo";
import { GetUserContext } from "../../UserContextProvider";

function CancelLeaveModel(props) {
  // console.log(props);
  const { userData } = useContext(GetUserContext);
  const details = userData || {};
  const { id = "" } = details;
  // console.log(id);

  const [loader, setLoader] = useState(false);
  const [cancelLeaveLoader, setCancelLeaveLoader] = useState(false);
  const [cancelVisible, setCancelVisible] = useState(false);
  const [dateTime, setDateTime] = useState();
  const [inpData, setInpData] = useState({
    id: "",
    requestType: "",
    leaves: "",
  });
  let leaveIds = [];

  useEffect(() => {
    if (props && props.leave) {
      const details = props.leave;
      setInpData({
        id: details.id,
        requestType: details.requestType,
        leaves: details.leaves,
      });
    }
  }, [props, props.leave]);
  // console.log(inpData);

  useEffect(() => {
    if (inpData.leaves && inpData.leaves) {
      setDateTime(inpData.leaves);
    }
  }, [inpData.leaves]);

  for (let i in dateTime) {
    leaveIds.push(dateTime[i].id);
  }

  const showModalReq = () => {
    setCancelLeaveLoader(true);
    setCancelVisible(true);
  };

  const handleCancelOk = (e) => {
    if (leaveIds && leaveIds.length) {
      // console.log(leaveIds);
      setLoader(true);
      client
        .mutate({
          mutation: CANCEL_LEAVE,
          variables: {
            ids: leaveIds,
          },
          refetchQueries: [
            {
              query: GET_ALL_LEAVE_REQUEST,
              variables: { userId: id },
              fetchPolicy: "network-only",
            },
          ],
        })
        .then((res) => {
          Modal.success({
            content: "Leave canceled successfully",
          });
          setLoader(false);
          setCancelLeaveLoader(false);
          setCancelVisible(false);
        })
        .catch((error) => {
          console.log(error);
          Modal.error({
            title: "Leave can not be canceled",
          });
          setLoader(false);
        });
    } else {
      Modal.error({
        title: "Leave Data not found",
      });
    }
  };

  const handleCancelCancel = (e) => {
    setCancelVisible(false);
    setCancelLeaveLoader(false);
    setInpData({
      id: props.leave.id,
      requestType: props.leave.requestType,
    });
    setDateTime(props.leave.leaves);
  };

  const handleRemove = (i) => {
    const dateTimeArray = dateTime;
    const values = [...dateTimeArray];
    values.splice(i, 1);
    setDateTime(values);
  };

  // console.log(leaveIds);
  // console.log(dateTime);

  return (
    <div>
      <Button
        color="#FFFFFF"
        bordercolor="#3C3C84"
        bgcolor="#3C3C84"
        marginleft="10px"
        loading={cancelLeaveLoader}
        onClick={showModalReq}
      >
        CANCEL LEAVE
      </Button>
      <Modal
        className="request"
        title="Cancel Leave Request"
        visible={cancelVisible}
        onApprove={handleCancelOk}
        onCancel={handleCancelCancel}
        footer={[
          <Button
            key="reject"
            color="#FFFFFF"
            bordercolor="#999999"
            bgcolor="#999999"
            onClick={handleCancelCancel}
          >
            IGNORE
          </Button>,
          <Button
            key="approve"
            color="#FFFFFF"
            bordercolor="#3C3C84"
            bgcolor="#3C3C84"
            onClick={handleCancelOk}
            loading={loader}
          >
            CANCEL LEAVE
          </Button>,
        ]}
      >
        <div>
          <Form>
            <LeaveType>
              <P color="#666666" fontsize="12px">
                Leave Type
              </P>
              <P>{inpData.requestType}</P>
            </LeaveType>
            {dateTime &&
              dateTime.length &&
              dateTime.map((field, idx) => {
                // console.log(field);
                return (
                  <DateTime key={`${field}-${idx}`}>
                    <div>
                      <P color="#666666" fontsize="12px">
                        Date
                      </P>
                      <P>{moment(field.date).format("DD/MM/YYYY")}</P>
                    </div>
                    <div>
                      <P color="#666666" fontsize="12px">
                        Time
                      </P>
                      <P>{field.type}</P>
                    </div>
                    <div>
                      {dateTime.length > 1 && (
                        <DeleteOutlined
                          onClick={() => handleRemove(idx)}
                          style={{ paddingTop: "32px" }}
                        />
                      )}
                    </div>
                  </DateTime>
                );
              })}
          </Form>
        </div>
      </Modal>
    </div>
  );
}

export default CancelLeaveModel;
