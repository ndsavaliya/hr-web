import React, { useContext, useEffect } from "react";
import { Route, Redirect } from "react-router-dom";
import { AppContext } from "./AppContextProvider";
import client from "../../../apollo";
import { GET_CURRENT_USER } from "../graphql/Queries";

function GuardedRoute(props) {
  const { component: Component, path } = props;
  const { dispatch } = useContext(AppContext);
  const authToken = localStorage.getItem("access_token");

  useEffect(() => {
    if (authToken) {
      async function fetchData() {
        client
          .query({
            query: GET_CURRENT_USER,
          })
          .then(({ data: { currentUser } }) => {
            if (currentUser) {
              dispatch({ type: "SET_CURRENT_USER", data: currentUser });
            } else {
              // window.location.href = '/logout'
            }
          })
          .catch(error => {
            console.error(error);
          });
      }
      fetchData();
    }
  }, [dispatch, authToken]);

  return (
    <Route
      path={path}
      render={props => {
        if (!authToken) return <Redirect to="/login" />;
        return <Component {...props} />;
      }}
    />
  );
}

export default GuardedRoute;
