import React, { useContext } from "react";
import {
  LeavesDetails,
  MyLeaveWrapper,
  // LeaveStatistics,
  ScrollableDivMyleaves,
  LeaveStatus,
} from "../css/MyLeavesStyles";
import { P, Divider, Badge /*, Statistic*/ } from "../../AntStyles";
import { GET_ALL_LEAVE_REQUEST } from "../graphql/Queries";
import { GetUserContext } from "../../UserContextProvider";
import moment from "moment";
import { useQuery } from "react-apollo";
import CancelLeaveModel from "./CancelLeaveModel";
// import CancelAllLeavesModel from "./CancelAllLeavesModel";

function MyLeaves() {
  const { userData } = useContext(GetUserContext);
  const details = userData || {};
  const { id = "" } = details;

  const { loading, error, data } = useQuery(GET_ALL_LEAVE_REQUEST, {
    variables: { userId: id },
    fetchPolicy: "network-only",
  });

  if (loading) return <div>loading</div>;
  if (error) return <div>error</div>;
  const allLeaves = data && data.getAllLeaveRequest;

  return (
    <MyLeaveWrapper>
      {/* <LeaveStatistics>
        <div>
          <Statistic title="Total Leave" value={12} />
        </div>
        <div>
          <Statistic title="Leave Used" value={4.5} precision={1} />
        </div>
        <div>
          <Statistic title="Leave Balance" value={7.5} precision={1} />
        </div>
      </LeaveStatistics>
      <Divider /> */}
      <LeavesDetails>
        <P color="#666666" fontsize="18px" fontweight="500">
          Leave Request Status
        </P>
        {/* <CancelAllLeavesModel /> */}
      </LeavesDetails>
      <Divider />
      <ScrollableDivMyleaves>
        {allLeaves && allLeaves.length
          ? allLeaves.map((leave, index) => (
              <div key={index}>
                <LeavesDetails>
                  <div>
                    <div>
                      <P color="#333333" fontsize="18px" fontweight="500">
                        {leave.requestType}
                      </P>
                      {leave.leaves && leave.leaves.length
                        ? leave.leaves.map((leave, index) => {
                            return (
                              <P
                                color="#666666"
                                fontsize="10px"
                                fontweight="300"
                                key={index}
                              >
                                <b>{leave.type}</b> on{" "}
                                <b>{moment(leave.date).format("DD/MM/YYYY")}</b>
                              </P>
                            );
                          })
                        : "No Dates available"}
                    </div>
                  </div>
                  <div>
                    {leave && leave.status && leave.status === "PENDING" ? (
                      <LeaveStatus>
                        <P color="#CCCCCC" fontsize="12px" fontweight="500">
                          waiting for response
                        </P>
                        <CancelLeaveModel leave={leave} />
                      </LeaveStatus>
                    ) : leave.status === "APPROVE" ? (
                      <LeaveStatus>
                        <Badge color="#BBD88C" />
                        <P color="#BBD88C" fontsize="12px" fontweight="500">
                          Approved
                        </P>
                        <CancelLeaveModel leave={leave} />
                      </LeaveStatus>
                    ) : (
                      <LeaveStatus>
                        <Badge color="#EFB9B9" />
                        <P color="#EFB9B9" fontsize="12px" fontweight="500">
                          Rejected
                        </P>
                      </LeaveStatus>
                    )}
                  </div>
                </LeavesDetails>
                <Divider />
              </div>
            ))
          : "No leaves"}
      </ScrollableDivMyleaves>
    </MyLeaveWrapper>
  );
}

export default MyLeaves;
