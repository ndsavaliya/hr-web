import React, { useEffect, useState } from "react";
import { Col } from "antd";
import { Avatar, P, Days } from "../../AntStyles";
import {
  EmployeeDetailsWrapper,
  EmpImgName,
  WeekData,
} from "../css/EmployeeStyles";
import { GET_USER_AVAILABILITY, GET_USER_PUNCHES } from "../graphql/Queries";
import client from "../../../apollo";
import { groupBy } from "lodash";
import moment from "moment";
import EmployeeLoginPopover from "./EmployeeLoginPopover";
import { UserOutlined } from "@ant-design/icons";

function Employee(props) {
  const [userAvailabilityData, setUserAvailabilityData] = useState([]);
  const [punchData, setPunchData] = useState({});
  const { userData } = props;
  const [userId, setUserId] = useState("");
  const [userStatus, setUserStatus] = useState("");
  const [userBgcolor, setUserBgcolor] = useState("");
  const weekDays = ["M", "T", "W", "T", "F", "S", "S"];
  let todayLeaveData;

  useEffect(() => {
    if (userData) {
      setUserId(userData.id);
      client
        .query({
          query: GET_USER_AVAILABILITY,
        })
        .then((res) => {
          res &&
            res.data &&
            res.data.userAvailability &&
            setUserAvailabilityData(res.data.userAvailability);
        })
        .catch((e) => {
          console.log(e);
        });
    }
  }, [userData]);

  useEffect(() => {
    if (userId) {
      client
        .query({
          query: GET_USER_PUNCHES,
          variables: { userId },
        })
        .then((res) => {
          res &&
            res.data &&
            res.data.userPunches &&
            setPunchData(res.data.userPunches);
        })
        .catch((e) => {
          console.log(e);
        });
    }
  }, [userId]);

  useEffect(() => {
    if (punchData && punchData.checkIn) {
      setUserStatus("#7DB523");
      setUserBgcolor("#FFFFFF");
    } else if (todayLeaveData) {
      setUserStatus("#E04C4C");
      setUserBgcolor("#FFF2F2");
    } else {
      setUserStatus("#999999");
      setUserBgcolor("#EEEEEE");
    }
  }, [punchData, todayLeaveData]);

  const handleWeek = (_day, index) => {
    const dayIndex = index + 1;
    const leaveForSameDay = weeklyLeave.find(
      (_leave) => _leave.dateInNumber === dayIndex,
    );

    let leaveclass;
    if (dayIndex === moment().get("day")) {
      if (leaveForSameDay) {
        userOnLeaveToday(leaveForSameDay);
        if (leaveForSameDay.type === "FIRST_HALF") {
          leaveclass = "firsthalf_leave current_day";
        } else if (leaveForSameDay.type === "SECOND_HALF") {
          leaveclass = "sechalf_leave current_day";
        } else if (leaveForSameDay.type === "FULL_DAY") {
          leaveclass = "full_leave current_day";
        }
      } else if (dayIndex === 6 || dayIndex === 7) {
        leaveclass = "holiday current_day";
      } else {
        if (punchData.checkIn) {
          leaveclass = "no_leave current_day";
        } else {
          leaveclass = "tomorrow current_day";
        }
      }
    } else if (dayIndex < moment().get("day")) {
      if (leaveForSameDay) {
        if (leaveForSameDay.type === "FIRST_HALF") {
          leaveclass = "firsthalf_leave";
        } else if (leaveForSameDay.type === "SECOND_HALF") {
          leaveclass = "sechalf_leave";
        } else if (leaveForSameDay.type === "FULL_DAY") {
          leaveclass = "full_leave";
        }
      } else if (dayIndex === 6 || dayIndex === 7) {
        leaveclass = "holiday";
      } else {
        leaveclass = "no_leave";
      }
    } else if (dayIndex > moment().get("day")) {
      if (dayIndex === 6 || dayIndex === 7) {
        leaveclass = "holiday";
      } else {
        leaveclass = "tomorrow";
      }
    }
    return <Days key={index} className={leaveclass} height_width="10px" />;
  };

  const userOnLeaveToday = (userOnLeaveToday) => {
    if (userOnLeaveToday) {
      todayLeaveData = userOnLeaveToday;
    }
  };

  const userWiseAvailibility =
    userAvailabilityData &&
    userAvailabilityData.length &&
    groupBy(userAvailabilityData, function (n) {
      const userId = n.user.id;
      return userId;
    });

  const usersIdArray = Object.keys(userWiseAvailibility);

  let weeklyLeave = [];
  usersIdArray &&
    usersIdArray.length &&
    usersIdArray.map((uid, key) => {
      if (uid === userId) {
        weeklyLeave = (userWiseAvailibility[uid] || []).map((_leave) => ({
          type: _leave.leaves[0].type,
          dateInNumber: moment(_leave.leaves[0].date).get("day"),
        }));
      }
    });

  return (
    <Col xs={8} sm={6} md={4} lg={3} xl={2}>
      <EmployeeLoginPopover
        userData={userData}
        weeklyLeave={weeklyLeave}
        punchData={punchData}
      >
        <EmployeeDetailsWrapper bgcolor={userBgcolor}>
          <EmpImgName>
            <div>
              {userData && userData.profileImage ? (
                <Avatar
                  size={50}
                  bordersizestylecolor={`2px solid ${userStatus}`}
                  src={userData && userData.profileImage}
                />
              ) : (
                <Avatar
                  size={50}
                  bordersizestylecolor={`2px solid ${userStatus}`}
                  icon={<UserOutlined />}
                />
              )}
            </div>
            <div>
              <P fontsize="12px" fontweight="500">
                <b>
                  {userData && userData.name ? userData.name : "Not Defined"}
                </b>
              </P>
            </div>
          </EmpImgName>
          <WeekData>
            {weekDays.map((_day, index) => handleWeek(_day, index))}
          </WeekData>
        </EmployeeDetailsWrapper>
      </EmployeeLoginPopover>
    </Col>
  );
}

export default Employee;
