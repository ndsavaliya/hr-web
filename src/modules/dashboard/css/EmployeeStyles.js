import styled from "styled-components";

export const flex = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
  font-family: "Poppins", sans-serif;
`;

export const EmployeeDetailsWrapper = styled(flex)`
  flex-direction: column;
  justify-content: space-around;
  background-color: ${props => props.bgcolor};
  border-radius: 10px;
  margin: 8px;
  padding: 10px 5px;
  cursor: pointer;
`;

export const EmpImgName = styled(flex)`
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  margin: 9px 0px;

  & > div > span {
    margin-bottom: 10%;
  }
`;

export const WeekData = styled(flex)`
  flex-wrap: wrap;
  justify-content: center;
  margin-bottom: 2px;

  &.empWeek {
    padding: 2%;
    justify-content: flex-start;
  }
`;

export const EmpPopupTitle = styled(flex)`
  flex-direction: column;
  width: 209px;
  height: auto;
`;

export const EmpPopupTitleDetail = styled(flex)`
  justify-content: stretch;
`;

export const NameDesc = styled.div`
  margin-left: 10px;

  & > h5 {
    font-weight: 10px;
    font-size: 7px;
  }
`;

export const NameIcon = styled(flex)`
  justify-content: space-between;
  align-content: space-between;
  height: 70px;

  & > .mailIcon {
    align-items: flex-end;
  }
`;

export const WeekDetail = styled.div`
  margin: 5% 0%;
`;

export const EmpPopupContent = styled(flex)`
  justify-content: space-between;
`;
