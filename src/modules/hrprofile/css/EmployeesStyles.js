import styled from "styled-components";

const flex = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
  font-family: "Poppins", sans-serif;
`;

export const EmployeesWrapper = styled(flex)`
  flex-direction: column;
`;

export const EmployeesHeader = styled(flex)`
  justify-content: space-between;
  align-items: center;
  padding: 15px 10px 10px 30px;
`;

export const EmployeesList = styled.div`
  padding: 1% 2%;
`;

export const UpdateBtn = styled(flex)`
  & > .updtbtn {
    margin-right: 4%;
  }
`;
