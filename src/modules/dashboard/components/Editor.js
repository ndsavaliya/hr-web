import React from "react";
import ReactQuill from "react-quill";

Editor.modules = {
  toolbar: [
    [{ size: [] }],
    ["bold", "italic", "underline", "strike"],
    [{ list: "ordered" }, { list: "bullet" }],
    ["link", "image"],
    ["clean"],
  ],
  clipboard: {
    // toggle to add extra line breaks when pasting HTML:
    matchVisual: false,
  },
};

Editor.formats = [
  "size",
  "bold",
  "italic",
  "underline",
  "strike",
  "list",
  "bullet",
  "link",
  "image",
];

function Editor(props) {
  const { editorHtml } = props;

  const handleChange = html => {
    props.handleChange(html);
  };

  return (
    <div>
      <ReactQuill
        onChange={handleChange}
        value={editorHtml}
        modules={Editor.modules}
        formats={Editor.formats}
      />
    </div>
  );
}

export default Editor;
