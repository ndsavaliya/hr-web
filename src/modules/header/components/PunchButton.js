import React, { useState } from "react";
import { withRouter } from "react-router-dom";
import { Button } from "../../AntStyles";
import { CREATE_PUNCH, PUNCH_OUT } from "../graphql/Mutations";
import client from "../../../apollo";

function PunchButton({ userId, punch, setPunch }) {
  const [loader, setLoader] = useState(false);
  let tempDate;
  let date;

  const handlePunchIn = () => {
    if (userId) {
      setLoader(true);
      tempDate = new Date();
      date =
        tempDate.getFullYear() +
        "-" +
        (tempDate.getMonth() + 1) +
        "-" +
        tempDate.getDate() +
        " " +
        tempDate.getHours() +
        ":" +
        tempDate.getMinutes() +
        ":" +
        tempDate.getSeconds();

      client
        .mutate({
          mutation: CREATE_PUNCH,
          variables: { userId, checkIn: date },
        })
        .then((res) => {
          setLoader(false);
          setPunch(true);
        })
        .catch((error) => {
          console.log(error);
          setLoader(false);
        });
    }
  };

  const handlePunchOut = () => {
    if (userId) {
      setLoader(true);
      tempDate = new Date();
      date =
        tempDate.getFullYear() +
        "-" +
        (tempDate.getMonth() + 1) +
        "-" +
        tempDate.getDate() +
        " " +
        tempDate.getHours() +
        ":" +
        tempDate.getMinutes() +
        ":" +
        tempDate.getSeconds();

      client
        .mutate({
          mutation: PUNCH_OUT,
          variables: { userId, checkOut: date },
        })
        .then((res) => {
          setLoader(false);
          setPunch(false);
        })
        .catch((error) => {
          console.log(error);
          setLoader(false);
        });
    }
  };

  return (
    <div>
      {punch ? (
        <Button
          color="#3C3C84"
          bordercolor="#FFFFFF"
          onClick={handlePunchOut}
          loading={loader}
        >
          PUNCH OUT
        </Button>
      ) : (
        <Button
          color="#3C3C84"
          bordercolor="#FFFFFF"
          onClick={handlePunchIn}
          loading={loader}
        >
          PUNCH IN
        </Button>
      )}
    </div>
  );
}

export default withRouter(PunchButton);
