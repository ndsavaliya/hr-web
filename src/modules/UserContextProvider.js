import React, { useEffect, useState } from "react";
import client from "../apollo";
import { GET_USER } from "./dashboard/graphql/Queries";

const GetUserContext = React.createContext(null);

function UserContextProvider(props) {
  const currentUser = JSON.parse(localStorage.getItem("current_user"));
  const [userData, setUserData] = useState({});
  const userId = currentUser && currentUser.id;

  useEffect(() => {
    if (userId) {
      client
        .query({
          query: GET_USER,
          variables: { id: userId },
        })
        .then(res => {
          setUserData(res.data.getUser);
        })
        .catch(e => {
          console.log(e);
        });
    }
  }, [userId]);

  return (
    <GetUserContext.Provider value={{ userData, setUserData }}>
      {props.children}
    </GetUserContext.Provider>
  );
}

export { UserContextProvider, GetUserContext };
