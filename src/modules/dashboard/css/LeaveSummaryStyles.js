import styled from "styled-components";

const flex = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
  font-family: "Poppins", sans-serif;
`;

export const LeaveSummaryWrapper = styled(flex)`
  flex-direction: column;
  background-color: #f0f0fa;
  padding: 5% 5%;
  border-radius: 4px;
  height: 500px;
  width: 100%;
`;

export const ScrollableLeaveSummaryDiv = styled.div`
  height: 83%;
  overflow-y: auto;
  padding-right: 10px;

  &::-webkit-scrollbar {
    width: 3px;
  }

  &::-webkit-scrollbar-track {
    border-radius: 3px;
  }

  &::-webkit-scrollbar-thumb {
    background: #cccccc;
    border-radius: 2px;
  }
`;

export const LeaveDetails = styled(flex)`
  justify-content: space-between;
  align-items: center;
  padding-right: 10px;
`;

export const ImgNameLeaves = styled(flex)`
  justify-content: stretch;
  align-items: center;
  margin: 0px 20px;
`;

export const Leaves = styled(flex)`
  flex-wrap: wrap;
`;
