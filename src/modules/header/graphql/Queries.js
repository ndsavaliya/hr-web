import gql from "graphql-tag";

export const GET_CURRENT_USER = gql`
  query getCurretUser {
    currentUser {
      id
      token
      punchStatus
    }
  }
`;

export const GET_USER = gql`
  query getUser($id: ID!) {
    getUser(id: $id) {
      id
      name
      profileImage
      role
      email
      birthdate
      password
      companyId
      isDeleted
      resetPasswordToken
      resetPasswordExpires
      token
      punchStatus
    }
  }
`;

export const GET_NOTIFICATION = gql`
  query getNotification {
    getNotification {
      id
      title
      isViewed
      createdAt
    }
  }
`;
