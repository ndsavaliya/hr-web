import gql from "graphql-tag";

export const CREATE_LEAVE_REQUEST = gql`
  mutation createLeaveRequestMutation(
    $requestType: String
    $leaves: [LeaveInput!]
    $assignment: [ID!]!
    $description: String
  ) {
    createLeaveRequest(
      input: {
        isCancelled: false
        requestType: $requestType
        leaves: $leaves
        assignment: $assignment
        description: $description
        approved: []
        rejected: []
      }
    ) {
      id
      requestType
      assignment
      leaves {
        leaveRequestId
        type
        date
      }
      description
    }
  }
`;

export const UPDATE_NOTIFICATION = gql`
  mutation updateNotificationMutation($id: ID) {
    updateNotification(where: { id: $id }) {
      isViewed
    }
  }
`;

export const RESPONSE_LEAVE_REQUEST = gql`
  mutation responseLeaveRequestMutation($id: ID, $status: String) {
    responseLeaveRequest(input: { id: $id, status: $status }) {
      id
      status
      assignment
      approved
      rejected
    }
  }
`;

export const CANCEL_LEAVE = gql`
  mutation cancelLeave($ids: [ID!]) {
    cancelLeave(input: { ids: $ids })
  }
`;

// export const CANCEL_LEAVE = gql`
//   mutation cancelLeaveMutation(
//     $id: ID
//     $leaveRequestId: ID
//     $type: String
//     $date: Date
//   ) {
//     cancelLeave(
//       input: {
//         id: $id
//         leaveRequestId: $leaveRequestId
//         type: $type
//         date: $date
//       }
//     )
//   }
// `;
