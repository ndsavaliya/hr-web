import gql from "graphql-tag";

export const CREATE_PUNCH = gql`
  mutation createPunchMutation($userId: ID!, $checkIn: DateTime) {
    createPunch(
      input: { userId: $userId, checkIn: $checkIn, status: CHECKIN }
    ) {
      id
      userId
      checkIn
      checkOut
      duration
      status
    }
  }
`;

export const PUNCH_OUT = gql`
  mutation punchOutMutation($userId: ID!, $checkOut: DateTime) {
    punchOut(
      input: { userId: $userId, checkOut: $checkOut, status: CHECKOUT }
    ) {
      id
      userId
      checkIn
      checkOut
      duration
      status
    }
  }
`;

export const UPDATE_NOTIFICATION = gql`
  mutation updateNotificationMutation($id: ID) {
    updateNotification(where: { id: $id }) {
      isViewed
    }
  }
`;
