import gql from "graphql-tag";

export const REGISTER = gql`
  mutation registerMutation($name: String, $email: String, $companyId: ID) {
    register(input: { name: $name, email: $email, companyId: $companyId }) {
      id
      name
      email
      companyId
    }
  }
`;

export const SET_PASSWORD = gql`
  mutation setPasswordMutation($registerToken: String!, $newPassword: String!) {
    setPassword(registerToken: $registerToken, newPassword: $newPassword) {
      id
      name
      email
      password
    }
  }
`;

export const LOGIN = gql`
  mutation login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      token
    }
  }
`;

export const FORGOT_PASSWORD = gql`
  mutation forgotPasswordLink($email: String!) {
    forgotPassword(email: $email) {
      id
      name
      email
    }
  }
`;

export const RESET_PASSWORD = gql`
  mutation resetPasswordMutation(
    $resetPasswordToken: String!
    $newPassword: String!
  ) {
    resetPassword(
      resetPasswordToken: $resetPasswordToken
      newPassword: $newPassword
    ) {
      id
      name
      email
      password
    }
  }
`;
