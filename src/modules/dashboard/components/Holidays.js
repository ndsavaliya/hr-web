import React, { useContext } from "react";
import { Divider, P, Days } from "../../AntStyles";
import {
  HolidaysWrapper,
  ScrollableDiv,
  DateName,
} from "../css/HolidaysStyles";
import { ALL_HOLIDAYS } from "../graphql/Queries";
import { useQuery } from "react-apollo";
import { GetUserContext } from "../../UserContextProvider";
import moment from "moment";
import { startCase, groupBy } from "lodash";

function Holidays() {
  const { userData } = useContext(GetUserContext);
  const details = userData || {};
  const { companyId = "" } = details;

  const { loading, error, data } = useQuery(ALL_HOLIDAYS, {
    variables: { companyId },
    fetchPolicy: "network-only",
  });

  // if (loading) return <div>loading</div>;
  // if (error) return <div>error</div>;
  const allHolidaysList = data && data.allHolidays;

  const currentTime = `${moment().format("YYYY-MM-DD")}T00:00:00.000Z`;
  // For all future holidays
  const nextHolidayArray =
    allHolidaysList && allHolidaysList.length
      ? allHolidaysList.filter((holiday) => holiday.date > currentTime)
      : "";

  // next five holidays
  let fiveHolidays = [];
  if (nextHolidayArray && nextHolidayArray.length) {
    for (let i = 0; i < 5; ++i) {
      //for less the 5 holidays
      if (i < nextHolidayArray.length) {
        fiveHolidays.push(nextHolidayArray[i]);
      }
    }
  }

  let monthWiseHoliday = {};
  if (fiveHolidays && fiveHolidays.length) {
    monthWiseHoliday = groupBy(fiveHolidays, function (n) {
      return moment(n.date).format("MMMM, YYYY");
    });
  }

  const monthArray = Object.keys(monthWiseHoliday);

  return (
    <HolidaysWrapper>
      <P color="#333333" fontsize="24px" fontweight="500">
        Upcoming Holidays
      </P>
      <Divider />
      <ScrollableDiv>
        {loading
          ? "loading"
          : error
          ? "error"
          : monthArray && monthArray.length
          ? monthArray.map((month, key) => (
              <div key={key}>
                <P color="#666666" fontsize="18px" fontweight="500">
                  {month}
                </P>
                <Divider />

                {(monthWiseHoliday[month] || []).map((_holiday, index) => (
                  <div key={index}>
                    <DateName>
                      <Days className="year_holiday" height_width="40px">
                        <P color="#FFFFFF" fontsize="18px" fontweight="500">
                          {moment(_holiday.date).format("DD")}
                        </P>
                      </Days>
                      <P color="#333333" fontsize="18px" fontweight="500">
                        {startCase(_holiday.name)}
                      </P>
                    </DateName>
                    <Divider />
                  </div>
                ))}
              </div>
            ))
          : "No Holidays Found"}
      </ScrollableDiv>
    </HolidaysWrapper>
  );
}

export default Holidays;
