import styled from "styled-components";

export const ScrollableDivNotification = styled.div`
  margin-top: 10px;
  max-height: 280px;
  overflow-y: auto;

  &::-webkit-scrollbar {
    width: 3px;
  }

  &::-webkit-scrollbar-track {
    border-radius: 3px;
  }

  &::-webkit-scrollbar-thumb {
    background: #cccccc;
    border-radius: 2px;
  }
`;

export const ViewAllNotification = styled.div`
  background-color: #ffffff;
  text-align: center;
  padding: 10px 5px;
`;
