import React from "react";
import { Row, Col } from "antd";
import LeaveSummary from "./components/LeaveSummary";
import Holidays from "./components/Holidays";
import LeaveRequests from "./components/LeaveRequests";
import EmployeesLoggedIn from "./components/EmployeesLoggedIn";
import { Input, P } from "../AntStyles";
import {
  DashboardWrapper,
  NoticeSearchWrapper,
  NoticeWrapper,
  EmpLogin,
} from "./DashboardStyles";

const { Search } = Input;

function Dashboard() {
  return (
    <DashboardWrapper>
      <NoticeSearchWrapper>
        <NoticeWrapper>
          <P color="#3C3C84" fontsize="12px" fontweight="500">
            <b>
              Holaa Bessie!!!, Today we have 2 Birthdays and 5 collegues on
              leave. Have a great day!
            </b>
          </P>
        </NoticeWrapper>
        <div>
          <Search placeholder="Search" />
        </div>
      </NoticeSearchWrapper>

      <EmpLogin>
        <Row>
          <EmployeesLoggedIn />
        </Row>
      </EmpLogin>

      <div>
        <Row>
          <Col xs={24} lg={6} style={{ padding: 10 }}>
            <Holidays />
          </Col>

          <Col xs={24} lg={9} style={{ padding: 10 }}>
            <LeaveRequests />
          </Col>

          <Col xs={24} lg={9} style={{ padding: 10 }}>
            <LeaveSummary />
          </Col>
        </Row>
      </div>
    </DashboardWrapper>
  );
}

export default Dashboard;
