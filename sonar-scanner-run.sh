#!/bin/bash
#
# Args: deploy.sh
#

wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.2.0.1873-linux.zip

unzip sonar-scanner-cli-4.2.0.1873-linux.zip

rm sonar-scanner-cli-4.2.0.1873-linux.zip

chmod 777 sonar-scanner-4.2.0.1873-linux/conf/sonar-scanner.properties

echo "sonar.host.url=$SONAR_HOST_URL \nsonar.exclusions=node_modules/**,coverage/** \nsonar.gitlab.project_id=https://gitlab.com/${CI_PROJECT_PATH}" >> sonar-scanner-4.2.0.1873-linux/conf/sonar-scanner.properties

chmod +x sonar-scanner-4.2.0.1873-linux/bin/sonar-scanner

sonar-scanner-4.2.0.1873-linux/bin/sonar-scanner \
  -Dsonar.projectKey=$DSONAR_PROJECTKEY \
  -Dsonar.sources=./src \
  -Dsonar.host.url=$SONAR_HOST_URL \
  -Dsonar.login=$DSONAR_LOGIN
