import styled from "styled-components";

const flex = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
  font-family: "Poppins", sans-serif;
`;

export const WorkScheduleWrapper = styled(flex)`
  flex-direction: column;
  width: 100%;
  background-color: #ffffff;
  padding: 1% 1%;
`;

export const WorkDaySchedule = styled(flex)`
  flex-direction: column;
  justify-content: space-between;
  padding: 2% 7%;
`;

export const TimeFlexibility = styled(flex)`
  flex-direction: column;
  justify-content: space-between;
  margin: 10px 0px;

  & > P {
    margin: 15px 0px;
  }
`;

export const WorkingHours = styled(flex)`
  flex-direction: column;
  justify-content: space-between;
  margin: 10px 0px;

  & > P {
    margin: 15px 0px;
  }
`;

export const Timing = styled(flex)`
  /* padding: 0px 10px; */
  margin-bottom: 10px;
`;

export const StartTime = styled(flex)`
  flex-direction: column;
  margin-left: 20px;
  margin-right: 10px;
`;

export const EndTime = styled(flex)`
  flex-direction: column;
  margin-right: 20px;
  margin-left: 10px;
`;

export const BreakFlexibility = styled(flex)`
  flex-direction: column;
  justify-content: space-between;
  margin: 10px 0px;

  & > P {
    margin: 15px 0px;
  }
`;

export const ButtonDiv = styled(flex)`
  margin-top: 20px;

  & > .cancelbtn {
    margin-right: 10px;
  }

  @media screen and (max-width: 500px) {
    justify-content: flex-end;
  }

  @media screen and (min-width: 501px) {
    margin-left: 250px;
  }
`;
