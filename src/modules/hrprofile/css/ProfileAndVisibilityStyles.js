import styled from "styled-components";

export const ProfileAndVisibilityWrapper = styled.div`
  width: 100%;
  background-color: #ffffff;
  padding: 4% 1%;
  font-family: "Poppins", sans-serif;
`;

export const ProfilePic = styled.div`
  height: 110px;
  width: 110px;
  cursor: pointer;
  overflow: hidden;
  border-radius: 50%;
`;

export const ChangeProfile = styled.div`
  background: linear-gradient(transparent 50%, rgba(0, 0, 0, 0.6) 50%);
  width: 100%;
  height: 100%;
  position: relative;
  display: flex;
  top: -100%;

  & > .uploadclass {
    margin-top: 13%;
    margin-left: 5%;
  }

  p {
    margin-top: 70%;
    margin-left: 20%;

    & > span > svg {
      margin-right: 18px;
      margin-left: 28px;
    }
  }
`;

export const ButtonDiv = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-end;
  align-items: stretch;
  align-content: stretch;

  & > .cancelbtn {
    margin-right: 5%;
  }
`;
