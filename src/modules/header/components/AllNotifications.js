import React from "react";
import { P, Divider, Button } from "../../AntStyles";
import { GET_NOTIFICATION } from "../graphql/Queries";
import { UPDATE_NOTIFICATION } from "../graphql/Mutations";
import { useQuery } from "react-apollo";
import {
  AllNotificationWrapper,
  NotificationBar,
  NotificationTitle,
  NotificationHeader,
} from "../css/AllNotificationsStyled";
import client from "../../../apollo";
import moment from "moment";

function AllNotifications() {
  const { loading, error, data, refetch } = useQuery(GET_NOTIFICATION, {
    fetchPolicy: "network-only",
  });
  const notificationData = data && data.getNotification;

  const handleDeleteNotification = item => {
    const { id = "" } = item;
    if (id) {
      client
        .mutate({
          mutation: UPDATE_NOTIFICATION,
          variables: {
            id,
          },
        })
        .then(res => {
          refetch();
        })
        .catch(error => {
          console.log(error);
        });
    }
  };

  if (loading) return <div>loading</div>;
  if (error) return <div>error</div>;

  return (
    <AllNotificationWrapper>
      <NotificationHeader>
        <P color="#3C3C84" fontsize="36px" fontweight="500">
          All Notifications
        </P>
        {/* <Button
          color="#FFFFFF"
          bordercolor="#3C3C84"
          bgcolor="#3C3C84"
          // loading={reqLeaveLoader}
          onClick={handleDeleteAllNotifications}
        >
          Clear All
        </Button> */}
      </NotificationHeader>
      <div>
        {notificationData && notificationData.length
          ? notificationData.map((_notification, index) => {
              const { title, createdAt } = _notification;
              return (
                <NotificationBar key={_notification.id}>
                  <P color="#666666" fontsize="13px" fontweight="500">
                    {createdAt
                      ? moment(createdAt).format("DD/MM/YYYY h:mm a")
                      : ""}
                  </P>
                  <Divider />
                  <NotificationTitle>
                    <P color="#333333" fontsize="16px" fontweight="500">
                      {title}
                    </P>
                    <Button
                      color="#FFFFFF"
                      bordercolor="#3C3C84"
                      bgcolor="#3C3C84"
                      // loading={reqLeaveLoader}
                      onClick={() => handleDeleteNotification(_notification)}
                    >
                      Clear
                    </Button>
                  </NotificationTitle>
                </NotificationBar>
              );
            })
          : "No Notifications Found"}
      </div>
    </AllNotificationWrapper>
  );
}

export default AllNotifications;
