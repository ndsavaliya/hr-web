import React from "react";
import { RulesWrapper } from "../css/RulesStyles";
import { P, Button, Divider } from "../../AntStyles";

function Rules() {
  return (
    <div>
      <RulesWrapper>
        <div>
          <P fontsize="18px">Manage Rules</P>
        </div>
        <div>
          <Button color="#FFFFFF" bordercolor="#3C3C84" bgcolor="#3C3C84">
            IMPORT RULE LIST
          </Button>
        </div>
      </RulesWrapper>
      <Divider />
    </div>
  );
}

export default Rules;
