import styled from "styled-components";

const flex = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
`;

export const CalendarWrapper = styled.div`
  width: 100%;
  /* height: 500px; */
  background-color: #ffffff;
  /* padding: 4% 1%; */
  font-family: "Poppins", sans-serif;
`;

export const CalendarDiv = styled.div`
  width: 100%;
  height: 250px;
  background-color: #ffffff;
  font-family: "Poppins", sans-serif;
`;

export const Legends = styled(flex)`
  justify-content: flex-end;
  align-items: center;
  align-content: center;
  margin: 0px 40px;
`;

export const LeaveType = styled(flex)`
  margin: 0px 9px;
`;

export const LeaveColor = styled(flex)`
  align-items: center;
  align-content: center;
  height: 16px;
  width: 16px;
  /* background: red; */
  margin: 0px 10px;

  &.present {
    background: #bbd88c;
  }

  &.fullDay {
    background: #efb9b9;
  }

  &.firstHalf {
    background: linear-gradient(90deg, #efb9b9 50%, #bbd88c 50%);
  }

  &.SecHalf {
    background: linear-gradient(90deg, #bbd88c 50%, #efb9b9 50%);
  }
`;
