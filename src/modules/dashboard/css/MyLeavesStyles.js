import styled from "styled-components";

const flex = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
  font-family: "Poppins", sans-serif;
`;

export const MyLeaveWrapper = styled(flex)`
  flex-direction: column;
`;

export const LeaveStatistics = styled(flex)`
  justify-content: space-around;
  padding-top: 10px;
`;

export const ScrollableDivMyleaves = styled.div`
  /* height: 150px; */
  height: 280px;
  overflow-y: auto;
  padding-right: 10px;

  &::-webkit-scrollbar {
    width: 3px;
  }

  &::-webkit-scrollbar-track {
    border-radius: 3px;
  }

  &::-webkit-scrollbar-thumb {
    background: #cccccc;
    border-radius: 2px;
  }
`;

export const LeavesDetails = styled(flex)`
  margin-top: 10px;
  justify-content: space-between;
  align-items: center;
  padding-right: 10px;
`;

export const LeaveStatus = styled(flex)`
  flex-wrap: wrap;
  align-items: center;
`;
