import React, { useEffect, useContext, useState } from "react";
import {
  Input,
  DatePicker,
  Button,
  Avatar,
  P,
  Form,
  Modal,
  Select,
} from "../../AntStyles";
import { Row, Col, Upload } from "antd";
import {
  ProfileAndVisibilityWrapper,
  ProfilePic,
  ChangeProfile,
  ButtonDiv,
} from "../css/ProfileAndVisibilityStyles";
import { UPDATE_USER } from "../graphql/Mutations";
import client from "../../../apollo";
import moment from "moment";
import { GetUserContext } from "../../UserContextProvider";
import { UserOutlined, LoadingOutlined } from "@ant-design/icons";

const { Option } = Select;

const currentUser = JSON.parse(localStorage.getItem("current_user"));
const userId = currentUser && currentUser.id;

function RegistrationForm() {
  const { userData, setUserData } = useContext(GetUserContext);
  const details = userData || {};
  const [form] = Form.useForm();
  const [loader, setLoader] = useState(false);
  const { name, role, email, birthdate = moment() } = details;

  useEffect(() => {
    if (userId && details) {
      const dateofbirth = birthdate ? moment(birthdate) : moment();
      form.setFieldsValue({
        fullname: name,
        email,
        jobpost: role,
        dateofbirth,
      });
    }
  }, [form, name, email, role, birthdate, details]);

  const handleFinish = (values) => {
    const { fullname, jobpost, email, dateofbirth } = values;
    setLoader(true);

    if (
      fullname &&
      jobpost &&
      email &&
      dateofbirth &&
      moment(dateofbirth).format("DD/MM/YYYY") !== moment().format("DD/MM/YYYY")
    ) {
      const bdate = `${moment(dateofbirth).format("YYYY-MM-DD")}T00:00:00.000Z`;
      client
        .mutate({
          mutation: UPDATE_USER,
          variables: {
            id: userId,
            name: fullname,
            role: jobpost,
            email,
            birthdate: bdate,
          },
        })
        .then((res) => {
          const updateDetails = res.data;
          const { name, role, email, birthdate } = updateDetails.updateUser;
          setUserData((prevState) => {
            return { ...prevState, name, role, email, birthdate };
          });
          Modal.success({
            title: "User Updated Successfully",
          });
          setLoader(false);
        })
        .catch((error) => {
          console.log(error);
          form.setFieldsValue({
            fullname: name,
            email,
            jobpost: role,
            dateofbirth: moment(birthdate),
          });
          Modal.error({
            content: "Data is invalid",
          });
          setLoader(false);
        });
    } else {
      Modal.error({
        content: "Data should not be empty",
      });
      setLoader(false);
    }
  };

  const handleCancel = () => {
    form.setFieldsValue({
      fullname: name,
      email,
      jobpost: role,
      dateofbirth: moment(birthdate),
    });
  };

  return (
    <Form form={form} onFinish={handleFinish} layout="vertical">
      <Form.Item
        label="Full Name"
        name="fullname"
        rules={[{ required: true, message: "Please input your full name!" }]}
      >
        <Input size="large" />
      </Form.Item>

      <Form.Item
        label="Job Post"
        name="jobpost"
        rules={[{ required: true, message: "Please select your job post!" }]}
      >
        <Select size="large">
          <Option value="Manager">Manager</Option>
          <Option value="Employee">Employee</Option>
        </Select>
      </Form.Item>

      <Form.Item
        label="E-mail"
        name="email"
        rules={[
          {
            type: "email",
            message: "The input is not valid E-mail!",
          },
          {
            required: true,
            message: "Please input your E-mail!",
          },
        ]}
      >
        <Input size="large" disabled />
      </Form.Item>

      <Form.Item
        label="Date of Birth"
        name="dateofbirth"
        rules={[
          {
            type: "object",
            required: true,
            message: "Please select birth date!",
          },
        ]}
      >
        <DatePicker
          style={{ width: "100%" }}
          size="large"
          format="DD/MM/YYYY"
          disabledDate={(current) => current && current > moment()}
        />
      </Form.Item>

      <Form.Item>
        <ButtonDiv>
          <Button
            className="cancelbtn"
            color="#FFFFFF"
            bgcolor="#999999"
            bordercolor="#999999"
            onClick={handleCancel}
          >
            Cancel
          </Button>
          <Button
            color="#FFFFFF"
            bgcolor="#3C3C84"
            bordercolor="#3C3C84"
            htmlType="submit"
            loading={loader}
          >
            Save
          </Button>
        </ButtonDiv>
      </Form.Item>
    </Form>
  );
}

function ProfileAndVisibility() {
  const { userData, setUserData } = useContext(GetUserContext);
  const details = userData || {};
  const [fileObj, setFileObj] = useState([]);
  const [loader, setLoader] = useState(false);
  const { profileImage } = details;

  useEffect(() => {
    if (fileObj && fileObj.length) {
      setLoader(true);
      const [file] = fileObj;
      client
        .mutate({
          mutation: UPDATE_USER,
          variables: {
            id: userId,
            profileImage: file,
          },
        })
        .then((res) => {
          setUserData((prevState) => {
            return {
              ...prevState,
              profileImage: res.data.updateUser.profileImage,
            };
          });
          Modal.success({
            title: "Profile Picture Updated Successfully",
          });
          setLoader(false);
        })
        .catch((error) => {
          console.log(error);
          Modal.error({
            content: "Profile picture cant not be updated",
          });
          setLoader(false);
        });
    }
  }, [fileObj, setUserData]);

  function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener("load", () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  const handleChange = (info) => {
    setLoader(true);
    console.log("info.file.originFileObj", info.file.originFileObj);
    getBase64(info.file.originFileObj, (imageUrl) => {
      let fileData = [info.file.originFileObj];
      setFileObj(fileData);
    });
  };

  return (
    <ProfileAndVisibilityWrapper>
      <Row>
        <Col
          xs={{ span: 8, offset: 8 }}
          sm={{ span: 8, offset: 8 }}
          md={{ span: 4, offset: 6 }}
          lg={{ span: 3, offset: 7 }}
          xl={{ span: 3, offset: 7 }}
        >
          <ProfilePic>
            {profileImage ? (
              <Avatar size={110} src={profileImage} />
            ) : (
              <Avatar size={110} icon={<UserOutlined />} />
            )}
            <ChangeProfile>
              <Upload
                customRequest={(_) => console.log(_)}
                className="uploadclass"
                onChange={handleChange}
              >
                {loader ? (
                  <P color="#FFFFFF" fontsize="18px">
                    <LoadingOutlined />
                    {/* uploading */}
                  </P>
                ) : (
                  <P color="#FFFFFF" fontsize="18px">
                    Change
                  </P>
                )}
              </Upload>
            </ChangeProfile>
          </ProfilePic>
        </Col>
        <Col xs={24} sm={24} md={10} lg={7} xl={7}>
          <RegistrationForm />
        </Col>
      </Row>
    </ProfileAndVisibilityWrapper>
  );
}

export default ProfileAndVisibility;
