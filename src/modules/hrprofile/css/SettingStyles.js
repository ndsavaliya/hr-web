import styled from "styled-components";

const flex = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
  font-family: "Poppins", sans-serif;
`;

export const SettingWrapper = styled.div`
  width: 100%;
  background-color: #ffffff;
  padding: 4% 1%;
`;

export const AllowDesNoti = styled(flex)`
  justify-content: space-between;
  align-items: center;
  background-color: #f0f0fa;
  padding: 1%;
  color: #666666;
`;

export const ButtonDiv = styled(flex)`
  justify-content: flex-end;

  & > .cancelbtn {
    margin-right: 5%;
  }
`;
