import React from "react";
import { Dropdown, P, Badge } from "../../AntStyles";
import { Menu } from "antd";
import { BellOutlined } from "@ant-design/icons";
import { GET_NOTIFICATION } from "../graphql/Queries";
import { UPDATE_NOTIFICATION } from "../graphql/Mutations";
import { useQuery } from "react-apollo";
import {
  ScrollableDivNotification,
  ViewAllNotification,
} from "../css/NotificationBellStyled";
import client from "../../../apollo";
import { Link } from "react-router-dom";

function NotificationBell() {
  const { loading, error, data, refetch } = useQuery(GET_NOTIFICATION, {
    fetchPolicy: "network-only",
  });
  const notificationData = data && data.getNotification;

  const handleNotificationClick = item => {
    const { id = "" } = item;
    if (id) {
      client
        .mutate({
          mutation: UPDATE_NOTIFICATION,
          variables: {
            id,
          },
        })
        .then(res => {
          refetch();
        })
        .catch(error => {
          console.log(error);
        });
    }
  };

  const notificationMenu = loading ? (
    <div>loading</div>
  ) : error ? (
    <div>error</div>
  ) : (
    <div>
      <ScrollableDivNotification>
        <Menu>
          {notificationData &&
            notificationData.length &&
            notificationData.map((item, index) => (
              <Menu.Item
                key={index}
                onClick={() => handleNotificationClick(item)}
              >
                <P fontsize="14px" fontweight="500">
                  {item && item.title}
                </P>
              </Menu.Item>
            ))}
        </Menu>
      </ScrollableDivNotification>
      <ViewAllNotification>
        <Link to="/all-notifications">
          <P fontsize="14px" fontweight="500">
            View All Notifications
          </P>
        </Link>
      </ViewAllNotification>
    </div>
  );

  return (
    <div>
      <Dropdown
        className="notification"
        overlay={notificationMenu}
        placement="bottomRight"
        trigger={["click"]}
      >
        <Badge dot={notificationData && notificationData.length}>
          <BellOutlined
            style={{ fontSize: "20px", color: "#FFFFFF", cursor: "pointer" }}
          />
        </Badge>
      </Dropdown>
    </div>
  );
}

export default NotificationBell;
