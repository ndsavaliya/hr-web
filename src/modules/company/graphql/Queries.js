import gql from "graphql-tag";

export const GET_COMPANY = gql`
  query getCompany($id: ID) {
    getCompany(where: { id: $id }) {
      id
      name
      location
      timezone
    }
  }
`;

export const GET_PUNCH_SETTING = gql`
  query getPunchSetting($companyId: ID!) {
    getPunchSetting(companyId: $companyId) {
      id
      companyId
      timeFlexibility
      startTime
      endTime
      breakTimeFlexibility
      breaks
    }
  }
`;

export const LEAVE_SETTING = gql`
  query getLeaveSetting($companyId: ID!) {
    getLeaveSetting(companyId: $companyId) {
      id
      companyId
      leaveTypes {
        id
        leaveSettingId
        type
        totalLeaves
        isPaid
      }
    }
  }
`;
