import React, { useEffect } from "react";
import { Switch, Route, withRouter } from "react-router-dom";
import Dashboard from "./Dashboard";
import HRProfile from "../hrprofile/HRProfile";
import OnboardCompany from "../company/OnboardCompany";
import Header from "../header/Header";
import { GET_CURRENT_USER } from "./graphql/Queries";
import client from "../../apollo";
import { UserContextProvider } from "../UserContextProvider";
import { useState } from "react";
import { NOTIFICATION_SUBSCRIPTION } from "./graphql/Subscriptions";
import { useSubscription } from "react-apollo";
import { notification, Spin } from "antd";
import AllNotifications from "../header/components/AllNotifications";
import { CompanyContextProvider } from "../CompanyContextProvider";

function Home(props) {
  const [userLoaded, setUserLoaded] = useState(false);
  const [punch, setPunch] = useState(false);
  const [currentUserData, setCurretUserData] = useState();
  useEffect(() => {
    client
      .query({
        query: GET_CURRENT_USER,
        fetchPolicy: "network-only",
      })
      .then((res) => {
        // console.log(res);
        if (res && res.data && res.data.currentUser) {
          localStorage.setItem(
            "current_user",
            JSON.stringify(res.data.currentUser),
          );
          setCurretUserData(res.data.currentUser);
          const condition = res.data.currentUser.punchStatus === "CHECKIN";
          setPunch(condition);
          setUserLoaded(true);
        }
      })
      .catch((e) => {
        console.log(e);
        localStorage.clear();
        props.history.push("/login");
      });
  }, [props.history, punch]);

  const { data } = useSubscription(NOTIFICATION_SUBSCRIPTION);

  // console.log(data);
  useEffect(() => {
    if (data) {
      const name =
        data &&
        data.notification &&
        data.notification.user &&
        data.notification.user.name
          ? data.notification.user.name
          : data.notification.user.email;
      notification["warning"]({
        message: `You have a notification from ${name}`,
        onClick: () => {
          console.log("Notification Clicked!");
        },
      });
    }
  }, [data]);

  return (
    <div>
      {userLoaded ? (
        <UserContextProvider>
          <CompanyContextProvider>
            <Header
              values={{ punch, setPunch }}
              currentUserData={currentUserData}
            />
            <Switch>
              <Route exact path="/" component={Dashboard} />
              <Route exact path="/hr-profile" component={HRProfile} />
              <Route exact path="/company-profile" component={OnboardCompany} />
              <Route
                exact
                path="/all-notifications"
                component={AllNotifications}
              />
            </Switch>
          </CompanyContextProvider>
        </UserContextProvider>
      ) : (
        <Spin
          size="large"
          tip="Loading Dashboard"
          style={{
            width: "3rem",
            height: "3rem",
            position: "fixed",
            left: "50%",
            top: "50%",
          }}
        />
      )}
    </div>
  );
}

export default withRouter(Home);
