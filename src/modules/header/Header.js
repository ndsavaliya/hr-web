import React, { useContext, useEffect, useState } from "react";
import { Link, withRouter } from "react-router-dom";
import {
  HeaderWrapper,
  HeaderTwo,
  HeaderDiv,
  ProfileDiv,
} from "./HeaderStyles";
import { Avatar, Dropdown, P } from "../AntStyles";
import { Menu } from "antd";
import { CaretDownOutlined } from "@ant-design/icons";
import PunchButton from "./components/PunchButton";
import { startCase } from "lodash";
import { GetUserContext } from "../UserContextProvider";
import NotificationBell from "./components/NotificationBell";
import { UserOutlined } from "@ant-design/icons";
import client from "../../apollo";
import { PUNCH_OUT } from "./graphql/Mutations";

function Header(props) {
  // console.log("header");
  const { punch, setPunch } = props.values;
  const { userData } = useContext(GetUserContext);
  const details = userData || {};
  const { name = "", profileImage = "", role = "" } = details;
  const [punchStatus, setPunchStatus] = useState("");
  const [userId, setUserId] = useState("");

  useEffect(() => {
    // console.log("header useEff");
    const currentUser = JSON.parse(localStorage.getItem("current_user"));
    const id = currentUser && currentUser.id;
    const punchData = currentUser && currentUser.punchStatus;
    setUserId(id);
    setPunchStatus(punchData);
  }, [props.currentUserData]);

  // console.log(punchStatus);

  const handleLogout = () => {
    console.log(punchStatus);
    let tempDate;
    let date;

    if (userId && punchStatus === "CHECKIN") {
      console.log("bvbdbdbb");
      tempDate = new Date();
      date =
        tempDate.getFullYear() +
        "-" +
        (tempDate.getMonth() + 1) +
        "-" +
        tempDate.getDate() +
        " " +
        tempDate.getHours() +
        ":" +
        tempDate.getMinutes() +
        ":" +
        tempDate.getSeconds();

      client
        .mutate({
          mutation: PUNCH_OUT,
          variables: { userId, checkOut: date },
        })
        .then((res) => {
          setPunch(false);
        })
        .catch((error) => {
          console.log(error);
        });
    }
    localStorage.clear();
    props.history.push("/login");
  };

  const menu = (
    <Menu>
      {role === "Admin" && (
        <Menu.Item>
          <Link to="/company-profile">
            <P fontsize="18px" fontweight="500">
              Onboard Company
            </P>
          </Link>
        </Menu.Item>
      )}
      <Menu.Item>
        <P fontsize="18px" fontweight="500" onClick={handleLogout}>
          Logout
        </P>
      </Menu.Item>
    </Menu>
  );

  return (
    <HeaderWrapper>
      <div>
        <Link to="/">
          <P color="#FFFFFF" fontsize="36px" fontweight="600">
            LOGO
          </P>
        </Link>
      </div>
      <HeaderTwo>
        <HeaderDiv>
          <PunchButton
            userId={userId}
            setPunch={setPunch}
            punch={punch}
            punchState={punch}
          />
        </HeaderDiv>
        <HeaderDiv>
          <NotificationBell />
        </HeaderDiv>
        <div>
          <Link to="/hr-profile">
            <ProfileDiv>
              {profileImage ? (
                <Avatar marginright="10px" src={profileImage} />
              ) : (
                <Avatar marginright="10px" icon={<UserOutlined />} />
              )}
              <P color="#FFFFFF" fontsize="12px" fontweight="500">
                {name ? startCase(name) : "No Defined"}
              </P>
            </ProfileDiv>
          </Link>
        </div>
        <div>
          <Dropdown overlay={menu} trigger={["click"]}>
            <CaretDownOutlined style={{ fontSize: "20px", color: "#FFFFFF" }} />
          </Dropdown>
        </div>
      </HeaderTwo>
    </HeaderWrapper>
  );
}

export default withRouter(Header);
