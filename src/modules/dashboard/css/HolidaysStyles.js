import styled from "styled-components";

const flex = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
  font-family: "Poppins", sans-serif;
`;

export const HolidaysWrapper = styled.div`
  background-color: #f0f0fa;
  padding: 5% 5%;
  border-radius: 4px;
  height: 500px;
  width: 100%;
`;

export const ScrollableDiv = styled.div`
  margin-top: 10px;
  height: 83%;
  overflow-y: auto;
  padding-right: 10px;

  ::-webkit-scrollbar {
    width: 3px;
  }

  ::-webkit-scrollbar-track {
    border-radius: 3px;
  }

  ::-webkit-scrollbar-thumb {
    background: #cccccc;
    border-radius: 2px;
  }
`;

export const DateName = styled(flex)`
  justify-content: stretch;
  align-items: center;
  margin: 0px 20px;
`;
