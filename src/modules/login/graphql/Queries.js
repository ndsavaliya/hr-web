import gql from "graphql-tag";

export const ALL_COMPANIES = gql`
  query getAllCompanies {
    allCompanies {
      id
      name
      location
      timezone
    }
  }
`;

export const GET_COMPANY = gql`
  query getCompany($name: String) {
    getCompany(where: { name: $name }) {
      id
      name
      location
      timezone
      deletedAt
    }
  }
`;

export const GET_CURRENT_USER = gql`
  query curretUser {
    currentUser {
      id
      name
      profileImage
      role
      email
      birthdate
      password
      companyId
      isDeleted
      resetPasswordToken
      resetPasswordExpires
      token
    }
  }
`;
