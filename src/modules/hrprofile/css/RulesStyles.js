import styled from "styled-components";

const flex = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
  font-family: "Poppins", sans-serif;
`;

export const RulesWrapper = styled(flex)`
  justify-content: space-between;
  align-items: center;
  padding: 15px 10px 10px 30px;
`;
