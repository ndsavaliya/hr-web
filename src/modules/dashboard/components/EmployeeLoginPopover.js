import React, { useState, useEffect } from "react";
import { Avatar, Popover, P, Days } from "../../AntStyles";
import {
  WeekData,
  EmpPopupTitle,
  EmpPopupTitleDetail,
  NameDesc,
  NameIcon,
  WeekDetail,
  EmpPopupContent,
} from "../css/EmployeeStyles";
import { MailFilled, UserOutlined } from "@ant-design/icons";

import moment from "moment";

function EmployeeLoginPopover(props) {
  const weeklyLeave = props.weeklyLeave;
  const { name = "", profileImage = "" } = props.userData;
  const { checkIn, checkOut } = props.punchData;
  const weekDays = ["M", "T", "W", "T", "F", "S", "S"];
  const [leaveType, setLeaveType] = useState("");
  let todayLeaveData = "";

  useEffect(() => {
    if (todayLeaveData) {
      setLeaveType(todayLeaveData.type);
    }
  }, [todayLeaveData]);

  let userStatus = checkIn ? "#7DB523" : todayLeaveData ? "#E04C4C" : "#999999";

  const handleWeek = (_day, index) => {
    const dayIndex = index + 1;
    const leaveForSameDay = weeklyLeave.find(
      (_leave) => _leave.dateInNumber === dayIndex,
    );

    let leaveclass;
    if (dayIndex === moment().get("day")) {
      if (leaveForSameDay) {
        userOnLeaveToday(leaveForSameDay);
        if (leaveForSameDay.type === "FIRST_HALF") {
          leaveclass = "firsthalf_leave current_day";
        } else if (leaveForSameDay.type === "SECOND_HALF") {
          leaveclass = "sechalf_leave current_day";
        } else if (leaveForSameDay.type === "FULL_DAY") {
          leaveclass = "full_leave current_day";
        }
      } else if (dayIndex === 6 || dayIndex === 7) {
        leaveclass = "holiday current_day";
      } else {
        if (checkIn) {
          leaveclass = "no_leave current_day";
        } else {
          leaveclass = "tomorrow current_day";
        }
      }
    } else if (dayIndex < moment().get("day")) {
      if (leaveForSameDay) {
        if (leaveForSameDay.type === "FIRST_HALF") {
          leaveclass = "firsthalf_leave";
        } else if (leaveForSameDay.type === "SECOND_HALF") {
          leaveclass = "sechalf_leave";
        } else if (leaveForSameDay.type === "FULL_DAY") {
          leaveclass = "full_leave";
        }
      } else if (dayIndex === 6 || dayIndex === 7) {
        leaveclass = "holiday";
      } else {
        leaveclass = "no_leave";
      }
    } else if (dayIndex > moment().get("day")) {
      if (dayIndex === 6 || dayIndex === 7) {
        leaveclass = "holiday";
      } else {
        leaveclass = "tomorrow";
      }
    }
    return (
      <Days key={index} className={leaveclass} height_width="20px">
        <P color="#666666" fontsize="10px" fontweight="500">
          {_day}
        </P>
      </Days>
    );
  };

  const userOnLeaveToday = (userOnLeaveToday) => {
    todayLeaveData = userOnLeaveToday;
  };

  const title = (
    <EmpPopupTitle>
      <NameIcon>
        <EmpPopupTitleDetail className="abc">
          <div>
            {profileImage ? (
              <Avatar
                size={50}
                bordersizestylecolor={`2px solid ${userStatus}`}
                src={profileImage}
              />
            ) : (
              <Avatar
                size={50}
                bordersizestylecolor={`2px solid ${userStatus}`}
                icon={<UserOutlined />}
              />
            )}
          </div>
          <NameDesc>
            <P color="#333333" fontsize="18px" fontweight="500">
              {name ? name : "Not Defined"}
            </P>
            <P color="#666666" fontsize="10px" fontweight="300">
              {moment().format("Z")}
            </P>
            <P color="#666666" fontsize="10px" fontweight="300">
              usually end work in 8 Hours
            </P>
            <P color="#FF0000" fontsize="8px" fontweight="300">
              {leaveType && `on ${leaveType} leave today`}
            </P>
          </NameDesc>
        </EmpPopupTitleDetail>
        <MailFilled className="mailIcon" />
      </NameIcon>
      <WeekDetail>
        <P color="#333333" fontsize="10px" fontweight="500">
          <b>Weekly Availability</b>
        </P>
        <WeekData className="empWeek">
          {weekDays.map((_day, index) => handleWeek(_day, index))}
        </WeekData>
      </WeekDetail>
    </EmpPopupTitle>
  );

  const content = (
    <EmpPopupContent>
      <div>
        <P color="#666666" fontsize="10px" fontweight="300">
          Logged in at
        </P>
        <P color="#666666" fontsize="10px" fontweight="300">
          Logged out at
        </P>
      </div>
      <div>
        <P color="#666666" fontsize="10px" fontweight="600">
          {checkIn ? checkIn : "-"}
        </P>
        <P color="#666666" fontsize="10px" fontweight="600">
          {checkOut ? checkOut : "-"}
        </P>
      </div>
    </EmpPopupContent>
  );

  return (
    <Popover
      className="popupclass"
      placement="bottom"
      title={title}
      content={content}
      trigger="click"
    >
      {props.children}
    </Popover>
  );
}

export default EmployeeLoginPopover;
