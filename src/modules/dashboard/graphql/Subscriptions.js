import gql from "graphql-tag";

export const NOTIFICATION_SUBSCRIPTION = gql`
  subscription notificationSubscription {
    notification {
      id
      assignment
      description
      user {
        name
        email
      }
    }
  }
`;
