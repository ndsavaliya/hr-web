import React from "react";
import Employees from "./Employees";
import Holidays from "./Holidays";
import Rules from "./Rules";
import { CompanyWrapper } from "../css/CompanyStyles";
import { TabBar } from "../../AntStyles";

const { TabPane } = TabBar;

function Company() {
  return (
    <CompanyWrapper>
      <TabBar defaultActiveKey="1" className="companytabflex">
        <TabPane tab="Employees" key="1">
          <Employees />
        </TabPane>
        <TabPane tab="Holidays" key="2">
          <Holidays />
        </TabPane>
        <TabPane tab="Rules" key="4">
          <Rules />
        </TabPane>
      </TabBar>
    </CompanyWrapper>
  );
}

export default Company;
