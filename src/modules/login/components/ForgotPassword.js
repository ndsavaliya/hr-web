import React, { useState } from "react";
import {
  MainDiv,
  Wrapper,
  ImgText,
  LoginForm,
  ButtonLink,
  SignupLink,
} from "../css/LoginStyled";
import { P, Form, Input, Button, Modal } from "../../AntStyles";
import { Link } from "react-router-dom";
import { FORGOT_PASSWORD } from "../graphql/Mutations";
import client from "../../../apollo";

function ForgotPasswordForm() {
  const [form] = Form.useForm();
  const [loader, setLoader] = useState(false);
  const handleFinish = values => {
    const { email } = values;
    setLoader(true);
    client
      .mutate({
        mutation: FORGOT_PASSWORD,
        variables: { email },
      })
      .then(data => {
        Modal.success({
          content: "Check your Mail",
        });
        form.setFieldsValue({
          email: "",
        });
        setLoader(false);
      })
      .catch(error => {
        console.log(error);
        Modal.error({
          title: "Invalid Email",
          content: "Input valid email",
        });
        setLoader(false);
      });
  };

  return (
    <Form form={form} onFinish={handleFinish} layout="vertical">
      <Form.Item
        label="Email"
        name="email"
        rules={[
          {
            type: "email",
            message: "The input is not valid E-mail!",
          },
          {
            required: true,
            message: "Please input your E-mail!",
          },
        ]}
      >
        <Input size="large" />
      </Form.Item>

      <Form.Item>
        <ButtonLink>
          <Button
            className="loginbtn"
            color="#FFFFFF"
            bgcolor="#3C3C84"
            bordercolor="#3C3C84"
            htmlType="submit"
            loading={loader}
          >
            Submit
          </Button>

          <SignupLink>
            <P color="#666666" fontsize="14px" fontweight="500">
              Go to
            </P>
            <Link to="/login">
              <P color="#3C3C84" fontsize="14px" fontweight="500">
                <b>Login</b>
              </P>
            </Link>
          </SignupLink>
        </ButtonLink>
      </Form.Item>
    </Form>
  );
}

function ForgotPassword() {
  return (
    <MainDiv>
      <Wrapper>
        <ImgText>
          <div className="welcomemsg">
            <P color="#FFFFFF" fontsize="48px">
              Welcome Back!
            </P>
            <P color="#CCCCCC" fontsize="18px">
              To keep Connected with us
            </P>
            <P color="#CCCCCC" fontsize="18px">
              please login here with your details
            </P>
          </div>
        </ImgText>
        <LoginForm>
          <P color="#000000" fontsize="36px" fontweight="500">
            Forgot Password?
          </P>
          <ForgotPasswordForm />
        </LoginForm>
      </Wrapper>
    </MainDiv>
  );
}

export default ForgotPassword;
