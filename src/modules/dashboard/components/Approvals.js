import React, { useContext } from "react";
import {
  ScrollableDivApprovals,
  LeavesDetails,
  ImgNameTime,
} from "../css/ApprovalsStyles";
import { P, Avatar, Divider } from "../../AntStyles";
import RespondLeaveModel from "./RespondLeaveModel";
import { GET_ALL_LEAVE_REQUEST } from "../graphql/Queries";
import moment from "moment";
import { GetUserContext } from "../../UserContextProvider";
import { useQuery } from "react-apollo";
import { UserOutlined } from "@ant-design/icons";
import { startCase } from "lodash";

function Approvals(props) {
  const { userData } = useContext(GetUserContext);
  const details = userData || {};
  const { id = "" } = details;

  const { loading, error, data } = useQuery(GET_ALL_LEAVE_REQUEST, {
    variables: { assignmentId: id },
    fetchPolicy: "network-only",
  });

  if (loading) return <div>loading</div>;
  if (error) return <div>error</div>;

  const allLeaveRequest = data.getAllLeaveRequest;

  return (
    <ScrollableDivApprovals className="abc">
      {allLeaveRequest && allLeaveRequest.length
        ? allLeaveRequest.map((leaveReq, index) => (
            <div key={index}>
              <LeavesDetails>
                <ImgNameTime>
                  {leaveReq && leaveReq.user && leaveReq.user.profileImage ? (
                    <Avatar
                      marginright="10px"
                      size={50}
                      src={leaveReq.user.profileImage}
                    />
                  ) : (
                    <Avatar
                      marginright="10px"
                      size={50}
                      icon={<UserOutlined />}
                    />
                  )}
                  <div>
                    <P color="#333333" fontsize="18px" fontweight="500">
                      {leaveReq && leaveReq.user && leaveReq.user.name
                        ? leaveReq.user.name
                        : leaveReq.user.email}
                    </P>
                    {leaveReq.leaves && leaveReq.leaves.length
                      ? leaveReq.leaves.map((leave, i) => {
                          return (
                            <P
                              color="#666666"
                              fontsize="10px"
                              fontweight="300"
                              key={i}
                            >
                              <b>{startCase(leave.type)}</b> on{" "}
                              <b>{moment(leave.date).format("DD/MM/YYYY")}</b>
                            </P>
                          );
                        })
                      : "No Dates available"}
                  </div>
                </ImgNameTime>
                <div>
                  <RespondLeaveModel leaveId={leaveReq.id} />
                </div>
              </LeavesDetails>
              <Divider />
            </div>
          ))
        : "No request yet found"}
    </ScrollableDivApprovals>
  );
}

export default Approvals;
